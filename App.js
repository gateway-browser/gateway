/* Gateway Browser
 * Copyright (C) 2020 Gateway Browser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/**
 * @format
 * @flow strict-local
 */

// React Libraries
import React from 'react';
import {StatusBar} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Redux Libraries
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
// import {persistReducer, persistCombineReducers} from 'redux-persist-immutable';
import {PersistGate} from 'redux-persist/integration/react';
import immutableTransform from 'redux-persist-transform-immutable';
import promiseMiddleware from 'redux-promise';
import thunk from 'redux-thunk';
// import {createLogger} from 'redux-logger';

import reducer from './src/reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  transforms: [immutableTransform()],
  blacklist: ['setup', 'webViewInjection'],
};
const persistedReducer = persistReducer(persistConfig, reducer);

// const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

function configureStore() {
  const enhancer = applyMiddleware(
    thunk,
    promiseMiddleware,
    // loggerMiddleware
  );
  return createStore(persistedReducer, enhancer);
}
const store = configureStore();
const persistor = persistStore(store);

import NavigationContainer from './src/views/NavigationContainer';
import ThemeStatusBar from './src/views/ThemeStatusBar';
import Toaster from './src/views/Toaster';

// TODO H - Notification Component
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeStatusBar />
        <NavigationContainer />
        <Toaster />
      </PersistGate>
    </Provider>
  );
};

export default App;
