#!/bin/bash

# Any copyright is dedicated to the Public Domain.
# http://creativecommons.org/publicdomain/zero/1.0/

set -eEu -o pipefail
shopt -s extdebug
IFS=$'\n\t'
trap 'onFailure $?' ERR

function onFailure() {
  echo "Unhandled script error $1 at ${BASH_SOURCE[0]}:${BASH_LINENO[0]}" >&2
  exit 1
}

rm ./nodejs-assets/nodejs-project/package-lock.json
rm -rf ./nodejs-assets/nodejs-project/node_modules/@babel/cli;

find ./nodejs-assets/nodejs-project/node_modules \
  -type f \
   ! \( \
    -name "*.node" \
    -o -path "*napi-macros/*" \
    -o -path "*node-gyp-build/*" \
    -o -path "*readable-stream/*" \
    -o -path "*timeout-refresh/*" \
    -o -path "*unordered-set/*" \
    -o -path "*inherits/*" \
    -o -path "*string_decoder/*" \
    -o -path "*util-deprecate/*" \
    -o -path "*safe-buffer/*" \
    -o -path "*process-nextick-args/*" \
    -o -path "*isarray/*" \
    -o -path "*core-util-is/*" \
    -o -path "*regenerator-runtime/*" \
    \) \
  -print0 | xargs -0 rm -rf

  # Delete symbolic links
  find ./nodejs-assets/nodejs-project/node_modules -depth -type l -delete
  # Delete empty directories
  find ./nodejs-assets/nodejs-project/node_modules -depth -type d -empty -delete
  find ./nodejs-assets/nodejs-project
