import {createReducer} from 'redux-immutablejs';
const {Map, List} = require('immutable');

import {getInfoKey} from '../lib/common';
import {
  hexagonCheckSvg,
  openFolderSvg,
  reactJsSvg,
  sourceSvg,
  webSvg,
} from '../svg';

const HEXAGON_CHECK_SVG = hexagonCheckSvg;
const FILES_SVG = openFolderSvg;
const SOURCE_SVG = sourceSvg;
const APP_SVG = reactJsSvg;
const SITE_SVG = webSvg;

export const protocols = createReducer(
  List([
    // Protocols can either be a JSON object like this or just a reference to a local directory that defines these attributes and can be accessed as a module withou the need to run a setup function
    // Hyper is done this way to demonstrate what the API looks like
    {
      name: 'Hyper',
      // RegExp for evaluating protocol in url
      prefix: '^hyper:|^dat:',
      // [Optional] SVG
      icon: {
        asset: HEXAGON_CHECK_SVG,
        // [Optional] - width / height
        iconRatio: 0.90625,
      },
      // [Optional] - Text for Protocol Info screen
      description:
        'Accessed on a secure distributed network that allows you to view its source and co-host its data.',
      // [Optional] - Reference ({'Module_Name':'Local_Path_or_Dat_URL'}) to modules the protocol needs
      //            - This format allows for all kinds of modules, including other protocols, to be detected
      dependencies: ['dat-sdk@2.0.0'],
      // [Optional] Function to do any necessary module-specific setup.
      // This is where one could definte its 'global.ProtocolName' variable (for custom functionality)
      setup: `async function(opts = {}) {
        console.log('[NODE] Setting up DAT protocol!');

        const DNS_PROVIDER_OPTIONS = [
          ['cloudflare-dns.com', 443, '/dns-query'],
          ['dns.quad9.net', 5053, '/dns-query'],
          ['doh.opendns.com', 443, 'dns-query'],
        ];
        let dnsProvider =
          DNS_PROVIDER_OPTIONS[Math.floor(Math.random() * DNS_PROVIDER_OPTIONS.length)];

        // TODO H - Roll custom implementation for toggling/handling persisted vs non-persisted drives/cores
        // TODO H - Set all Hyper storage to within PERM_DIR and/or TEMP_DIR
        const datOptions = Object.assign({
          // In order to connect to hyperdrive-daemon and beaker
          // applicationName: 'hyperdrive-daemon',
          // identityName: 'hyperdrive-daemon',
          dnsOpts: {
            dnsHost: dnsProvider[0],
            dnsPort: dnsProvider[1],
            dnsPath: dnsProvider[2],
          },
          // TODO H - BUG: Inconsistent hyperdrive retrieval
          //          TRY: Delete corestores for 'dat-sdk' and 'hyperdrive-daemon' to make sure no artifacts are messing things up
          swarmOpts: {
            announceLocalNetwork: true,
            preferredPort: 49737,
            maxPeers: 256,
            // Hyperdrive Daemon
            // announceLocalAddress: true,
            // preferredPort: 42666,
            // announce: true,
            // extensions: [],
          },
          corestoreOpts: {
            sparse: true,
            stats: true,
            ifAvailable: true,
          },
        }, opts);
        const Dat = await DatSDK(datOptions);
        Dat.peersockets = new Peersockets(Dat._swarm);
        Dat.watchStreams = new Map();

        return Dat;
      }`,
      // [Optional] Function for any special results for parseURL
      parseURL: `async function(url) {
        if (typeof url === 'string') {
          url = basicParseURL(url);
        }
        console.log('[NODE] URL read for parse!', url);
        const {hostname, href} = url;
        let key;
        let version = null;
        let options = {};

        if (hostname.indexOf('.') === -1) {
          const hostnameParts = hostname.split('+');
          key = hostnameParts[0];
          if (hostnameParts[1] && hostnameParts[1] !== '') {
            version = hostnameParts[1];
            if (version.indexOf('-') !== -1) {
              // Make sure Hyperdrive is just one version and not a version range
              const minVersion = version.split('-')[0];
              if (!isNaN(minVersion)) {
                // Version Number
                version = Number(minVersion);
              }
            } else if (!isNaN(version)) {
              // Version Number
              version = Number(version);
            }
          }
        } else {
          const hostnameParts = hostname.split('.');
          const subdomain = hostnameParts[0];
          const BASE_32_KEY_LENGTH = 52;
          if (subdomain.length === BASE_32_KEY_LENGTH) {
            key = hexTo32.decode(subdomain);
          } else {
            key = await Hyper.resolveName('dat://' + hostname);
          }
        }

        const optionsSplit = href.split('?');
        if (optionsSplit[1]) {
          const subOptions = optionsSplit[1].split('&');
          for (var i = 0; i < subOptions.length; i++) {
            const option = subOptions[i];
            if (option.indexOf('=') !== -1) {
              const keyPair = option.split('=');
              if (keyPair.length === 2) {
                options[keyPair[0]] = keyPair[1];
              }
            } else {
              options[option] = true;
            }
          }
        }

        return {
          key,
          version,
          options,
        }
      }`,
      // Methods that can be accessed via global.fetch
      // Generators are not currently supported
      // TODO H - Support mounts: For 'files', add some value to mount elements in contents array like 'link: 'hyper://mountUrl''. This prop will indicate to files explorer to render a link/redirect icon and tapping will just navigate with a 'files' context
      methods: {
        // Generic data retrieval
        // TODO H - Make methods more consistent with Mauve's proposal: https://github.com/cliqz-oss/dat-webext/issues/159
        GET: `async function*(request) {
          if (!request.headers) {
            request.headers = {};
          }
          if (!request.headers['Hyper-Options']) {
            request.headers['Hyper-Options'] = {persist: false};
          }
          if (
            request.headers['Child-Type'] &&
            typeof request.headers['Child-Type'] !== 'string'
          ) {
            throw new Error('The type parameter must be a string');
          }
          if (
            request.headers['Mount-URL'] &&
            typeof request.headers['Mount-URL'] !== 'string'
          ) {
            throw new Error('The mount parameter must be a string');
          }
          if (
            request.headers.Metadata &&
            typeof request.headers.Metadata !== 'object'
          ) {
            throw new Error('The metadata parameter must be an object');
          }

          const {
            key,
            version,
            options: parsedOptions,
            pathname,
          } = await parseURL(request.url);
          console.log('[NODE] Hyper GET version:', version);
          const {Hyperdrive, Hypercore} = Hyper;

          // TODO H - Hypercores
          let hyper = await Hyperdrive(
            key,
            Object.assign(request.headers['Hyper-Options'], parsedOptions),
          );
          const isReady = new Promise((resolve, reject) => {
            hyper.ready(readyError => {
              if (readyError) {
                reject(readyError);
              } else {
                resolve();
              }
            });
          });
          await isReady;
          await hyper.ready();

          // try {
          //   const test = await hyper.readdir('/');
          //   console.log('[NODE] Hyper Test Result:', test);
          //   if (test === undefined || test === null || test.length === 0) {
          //     throw new Error('Hyperdrive does not appear to exist.');
          //   }
          // } catch (e) {
          //   await hyper.destroyStorage();
          //   throw e;
          // }

          gateway.setSiteInfo(
            request.url,
            {
              peers: hyper.peers
                ? hyper.peers.map(peer => peer.stream.remoteAddress)
                : [],
              version: hyper.version,
            },
            true,
          );
          gateway.setSiteInfo(request.url, {
            display: [
              {
                key: 'version',
                alias: 'v{__VALUE__} (Latest)',
              },
              {
                key: 'peers.length',
                alias: '{__VALUE__} Peer(s)',
                // TODO L - icon: [SVG],
              },
            ],
          });

          // TODO H - TEMP: Remove until watching is cancelled when not needed
          // if (request.headers['Watch'] && !Hyper.watchStreams.has(key)) {
          //   var networkStream = pda.createNetworkActivityStream(hyper);
          //   networkStream.on('data', ([event, args]) => {
          //     if (event === 'network-changed') {
          //       gateway.setSiteInfo(request.url, {
          //         peers: hyper.peers ? hyper.peers.map(peer => peer.stream.remoteAddress) : [],
          //       }, true, true);
          //     }
          //   });
          //   Hyper.watchStreams.set(key, networkStream);
          //   hyper.on('update', () => {
          //     console.log('[NODE] Hyper Version Updated:', hyper.version);
          //     gateway.setSiteInfo(request.url, {version: hyper.version}, true, true);
          //   });
          // }

          if (version !== null) {
            if (typeof version === 'number') {
              hyper = await hyper.checkout(version);
            } else {
              hyper = await hyper.getTaggedVersion(version);
            }
          }

          if (
            request.headers['Content-Type'] &&
            request.headers['Content-Type'].indexOf('application/hyper') === 0
          ) {
            yield hyper;
          }

          if (hyper.length) {
            // TODO H - Handle hypercore
            //        - If Hypercore: Default to latest in feed (head)
            //        - Unless version (get) or range (getBatch) is specified
            if (request.headers.Stream) {
            } else {
            }
          } else {
            const stats = await pda.stat(hyper, pathname);
            if (stats.isFile()) {
              if (request.headers['Stream']) {
                // Promise Method
                // const readStream = hyper.createReadStream(pathname, request.headers['Method-Options']);
                // return readStream;
                // NOTE - Try this implementation if the other generator fails
                // async function* streamGenerator(stream) {
                //   for await (const chunk of stream) {
                //     yield chunk;
                //   }
                // }
                // NOTE - Last async generator attempt
                async function* streamAsyncIterable(stream) {
                  const reader = stream.getReader();
                  try {
                    while (true) {
                      const { done, value } = await reader.read();
                      if (done) {
                        return;
                      }
                      yield value;
                    }
                  } finally {
                    reader.releaseLock();
                  }
                }
                for await (const chunk of streamAsyncIterable(readStream)) {
                  yield chunk;
                }
              } else {
                yield hyper.readFile(pathname, request.headers['Method-Options']);
              }
            } else {
              yield pda.readdir(hyper, pathname, request.headers['Method-Options']);
            }
          }
        }`,
        QUERY: `async function*(request) {
          console.log('[NODE] Fetch QUERY:', request);
          if (!request.headers) {
            request.headers = {};
          }
          if (!request.headers['Hyper-Options']) {
            request.headers['Hyper-Options'] = {persist: false};
          }
          if (
            request.headers['Child-Type'] &&
            typeof request.headers['Child-Type'] !== 'string'
          ) {
            throw new Error('The type parameter must be a string');
          }
          if (
            request.headers['Mount-URL'] &&
            typeof request.headers['Mount-URL'] !== 'string'
          ) {
            throw new Error('The mount parameter must be a string');
          }
          if (
            request.headers.Metadata &&
            typeof request.headers.Metadata !== 'object'
          ) {
            throw new Error('The metadata parameter must be an object');
          }

          const {
            key,
            version,
            options: parsedOptions,
            pathname,
          } = await parseURL(request.url);
          const {Hyperdrive, Hypercore} = Hyper;

          let hyper = await Hyperdrive(
            key,
            Object.assign(request.headers['Hyper-Options'], parsedOptions),
          );
          const isReady = new Promise((resolve, reject) => {
            hyper.ready(readyError => {
              if (readyError) {
                reject(readyError);
              } else {
                resolve();
              }
            });
          });
          await isReady;

          if (version !== null) {
            if (typeof version === 'number') {
              hyper = await hyper.checkout(version);
            } else {
              hyper = await hyper.getTaggedVersion(version);
            }
          }

          // parse the pattern into a set of ops
          var matches = [];
          let acc = [];
          let ops = [];
          for (let part of pathname.split('/')) {
            if (part.includes('*')) {
              ops.push(['push', acc.filter(Boolean).join('/')]);
              ops.push(['match', part]);
              acc = [];
            } else {
              acc.push(part);
            }
          }
          if (acc.length) {
            ops.push(['push', acc.join('/')]);
          }

          console.log('[NODE] QUERY parsed pattern:', ops, acc);

          // run the ops to assemble a list of matching paths
          var workingPaths = [
            {
              name: '/',
              innerPath: '/',
              localDriveKey: hyper.key.toString('hex'),
            },
          ];
          for (let i = 0; i < ops.length; i++) {
            let op = ops[i];
            let isLastOp = i === ops.length - 1;
            let newWorkingPaths = [];
            if (op[0] === 'push') {
              // add the given segment to all working paths
              if (isLastOp) {
                newWorkingPaths = await Promise.all(
                  workingPaths.map(async workingPath => {
                    var bname = require('path').basename(op[1]);
                    var folderpath = op[1].slice(0, bname.length * -1);
                    let readdirpath = (
                      workingPath.name +
                      '/' +
                      folderpath
                    ).replace('//', '/');
                    console.log('[NODE] QUERY readdirpath:', readdirpath);
                    let readdiropts = {includeStats: true};
                    let items = await pda
                      .readdir(hyper, readdirpath, readdiropts)
                      .catch(err => []);
                    let item = items.find(item => item.name === bname);
                    if (!item) {
                      return undefined;
                    }
                    item.localDriveKey = item.mount
                      ? item.mount.key.toString('hex')
                      : workingPath.localDriveKey;
                    item.name = (
                      workingPath.name +
                      '/' +
                      folderpath +
                      '/' +
                      item.name
                    ).replace('//', '/');
                    return item;
                  }),
                );
                newWorkingPaths = newWorkingPaths.filter(Boolean);
              } else {
                newWorkingPaths = workingPaths.map(v => ({
                  name: (v.name + '/' + op[1]).replace('//', '/'),
                  innerPath: v.innerPath,
                  localDriveKey: v.localDriveKey,
                  stat: v.stat,
                  mount: v.mount,
                }));
              }
            } else if (op[0] === 'match') {
              // compile a glob-matching regex from the segment
              var re = new RegExp(
                '^' + op[1].replace(/\\*/g, '[^/]*') + '$',
                'i',
              );

              console.log('[NODE] QUERY workingPaths:', workingPaths);

              // read the files at each working path
              for (let workingPath of workingPaths) {
                let items = await pda
                  .readdir(hyper, workingPath.name, {includeStats: true})
                  .catch(e => []);
                for (let item of items) {
                  // add matching names to the working path
                  if (re.test(item.name)) {
                    item.localDriveKey = item.mount
                      ? item.mount.key.toString('hex')
                      : workingPath.localDriveKey;
                    item.name = (workingPath.name + '/' + item.name).replace(
                      '//',
                      '/',
                    );
                    newWorkingPaths.push(item);
                  }
                }
              }
            }
            workingPaths = newWorkingPaths;
          }

          console.log('[NODE] QUERY Stretch 1');

          // emit the results
          for (let result of workingPaths) {
            matches.push(result);
          }

          async function chunkMapAsync(arr, chunkSize, cb) {
            const resultChunks = [];
            for (let chunk of chunkArray(arr, chunkSize)) {
              resultChunks.push(await Promise.all(chunk.map(cb)));
            }
            return resultChunks.flat();
          }
          function chunkArray(arr, chunkSize) {
            const result = [];
            for (let i = 0; i < arr.length; i += chunkSize) {
              result.push(arr.slice(i, i + chunkSize));
            }
            return result;
          }

          var results = [];

          await chunkMapAsync(matches, 100, async item => {
            let path = item.name;
            let stat = item.stat;
            let localDriveKey = item.localDriveKey;
            let innerPath = item.innerPath;

            var type = 'file';
            if (stat.mount && stat.mount.key) {
              type = 'mount';
            } else if (stat.isDirectory()) {
              type = 'directory';
            }

            if (
              request.headers['Child-Type'] &&
              type !== request.headers['Child-Type']
            ) {
              return;
            }
            if (
              request.headers['Mount-URL'] &&
              (type !== 'mount' ||
                stat.mount.key.toString('hex') !== request.headers['Mount-URL'])
            ) {
              return;
            }
            if (request.headers.Metadata) {
              let metaMatch = true;
              for (let k in request.headers.Metadata) {
                if (stat.metadata[k] !== request.headers.Metadata[k]) {
                  metaMatch = false;
                  break;
                }
              }
              if (!metaMatch) {
                return;
              }
            }

            var drive = 'hyper://' + localDriveKey;
            results.push({
              type,
              path,
              url: drive + '/' + innerPath,
              stat,
              drive: 'hyper://' + localDriveKey,
              mount:
                type === 'mount'
                  ? 'hyper://' + stat.mount.key.toString('hex')
                  : undefined,
            });
          });

          console.log('[NODE] QUERY Stretch 2. Sorting...');

          if (request.headers.Sort === 'name') {
            results.sort((a, b) =>
              request.headers.Reverse
                ? require('path')
                    .basename(b.path)
                    .toLowerCase()
                    .localeCompare(
                      require('path')
                        .basename(a.path)
                        .toLowerCase(),
                    )
                : require('path')
                    .basename(a.path)
                    .toLowerCase()
                    .localeCompare(
                      require('path')
                        .basename(b.path)
                        .toLowerCase(),
                    ),
            );
          } else if (request.headers.Sort === 'mtime') {
            results.sort((a, b) =>
              request.headers.Reverse
                ? b.stat.mtime - a.stat.mtime
                : a.stat.mtime - b.stat.mtime,
            );
          } else if (request.headers.Sort === 'ctime') {
            results.sort((a, b) =>
              request.headers.Reverse
                ? b.stat.ctime - a.stat.ctime
                : a.stat.ctime - b.stat.ctime,
            );
          }

          if (request.headers.Offset && request.headers.Limit) {
            results = results.slice(
              request.headers.Offset,
              request.headers.Offset + request.headers.Limit,
            );
          } else if (request.headers.Offset) {
            results = results.slice(request.headers.Offset);
          } else if (request.headers.Limit) {
            results = results.slice(0, request.headers.Limit);
          }

          // HACK - MD Files
          // results = results.map(res => {
          //   if (res.path.endsWith('.md')) {
          //     res.path = res.path.replace('.md', '-md.html');
          //   }
          //   return res;
          // });

          console.log('[NODE] QUERY Results:', results);

          if (results.length === 1) {
            yield results[0];
          }
          yield results;
        }`,
        // For browser navigation
        // TODO H - Possibly move outside of fetch since it isn't really consistent with fetch behavior
        LOAD: `async function*(request) {
          console.log('[NODE] Load Hyper URL:', request);

          if (!request.headers) {
            request.headers = {};
          }

          if (request.headers.Navigate) {
            gateway.setNavigationProgress(request.headers['Tab-Index'], 0);
          }

          // TODO H - Replace with PARSED_URL
          let {protocol, key, version, pathname} = await parseURL(request.url);
          if (!pathname || pathname === '') {
            pathname = '/';
          }

          console.log('[NODE] URL Parsed:', key, version, pathname);

          const fetchHyper = await fetch(request.url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyper',
              'Hyper-Options': request.headers['Hyper-Options'],
              Watch: true,
            },
          });
          let hyper = await fetchHyper.body.next();
          if (hyper.value) {
            hyper = hyper.value;
          }

          if (request.headers.Navigate) {
            gateway.setNavigationProgress(request.headers['Tab-Index'], 0.5);
          }

          let newHyperInfo = {
            browserContexts: ['feed'],
            writable: hyper.isCheckout ? false : hyper.writable,
            display: [
              {
                key: 'writable',
                alias: {
                  true: {
                    label: 'Editable',
                    // TODO L - icon: [SVG],
                  },
                  false: {
                    label: 'Read-Only',
                    // TODO L - icon: [SVG],
                  },
                },
              },
            ],
          };

          if (hyper.version && hyper.version > Number(version)) {
            version = null;
          }

          const versionStr = version ? '+' + version : '';
          const baseUrl = protocol + '//' + key;
          const versionUrl = baseUrl + versionStr;

          let context = request.headers.Context || 'site';
          if (context !== 'site' && context !== 'files' && context !== 'feed') {
            context = 'site';
          }

          if (hyper.length) {
            context = 'feed';
            // TODO H - Return feed
          } else {
            let results = {scopeInfo: []};
            if (version !== null && version !== undefined) {
              results.scopeInfo = [
                {
                  key: 'version',
                  value: version,
                  alias: 'v{__VALUE__}',
                },
              ];
            }
            let manifest = {};
            try {
              manifest = await pda.readManifest(hyper);
              newHyperInfo = Object.assign(newHyperInfo, manifest);
            } catch (e) {
              console.log('[NODE] Manifest Read Error:', e);
            }
            const webRoot = manifest.web_root || '';
            const webPath = webRoot + pathname;
            let path = context === 'site' ? webPath : pathname;
            if (path.indexOf('/') === -1) {
              path = '/' + path;
            }
            let stats;
            let usingUiFrontend = false;
            let hasApp = false;

            if (
              context === 'app' &&
              path.indexOf('.rn') !== -1 &&
              path.replace(/\\//g, '') !== '.rn'
            ) {
              context === 'site'
            }

            try {
              console.log('[NODE] Attempting stat for path:', path, 'from pathname:', pathname);
              stats = await pda.stat(hyper, path);
            } catch (statError) {
              console.log('[NODE] First stat error:', statError);
              // Fallback if a file in a nested directory doesn't have its extension in the path
              if (path === '') {
                path = '/';
              }
              let splitPath = path.split('/');
              let foundElements = [];
              let parentContents = [];
              let pathParent = '';
              for (var i = 0; i < splitPath.length; i++) {
                const subPath = splitPath[i];
                if (subPath === '' && i === 0) {
                  parentContents = await pda.readdir(hyper, '/');
                  console.log('[NODE] Initial parentContents:', parentContents);
                } else if (subPath !== '') {
                  console.log('[NODE] Finding element named:', subPath);
                  foundElements = parentContents.filter(element => {
                    return element.includes(subPath);
                  }).sort((a, b) => {
                    return a.length - b.length
                  });
                  if (foundElements.length === 0) {
                    break;
                  } else if (i < splitPath.length - 1) {
                    pathParent = pathParent + '/' + subPath;
                    parentContents = await pda.readdir(hyper, pathParent);
                    console.log('[NODE] Next parentContents:', parentContents, 'From pathParent:', pathParent);
                  }
                }
              }
              console.log(
                '[NODE] Found fallback elements:',
                foundElements,
                'From parentContents:',
                parentContents,
                'And pathParent:',
                pathParent,
                'From path:',
                path,
              );
              if (foundElements.length === 0) {
                throw new Error('File not found for: ' + path);
              }
              const foundHtml = foundElements.find(element =>
                element.endsWith('.html'),
              )
              const foundMd = foundElements.find(element =>
                element.endsWith('.md'),
              )
              if (context === 'site' && foundHtml) {
                path = (pathParent + '/' + foundHtml).replace('//', '/');
              } else if (context === 'site' && foundMd) {
                path = (pathParent + '/' + foundMd).replace('//', '/');
              } else {
                path = (pathParent + '/' + foundElements[0]).replace('//', '/');
              }
              try {
                stats = await pda.stat(hyper, path);
              } catch (fallbackStatError) {
                if (context === 'site' && request.headers.Navigate) {
                  // To be consistent with Beaker behavior, attempt to load /.ui frontend if file isn't found
                  path = '/.ui';
                  stats = await pda.stat(hyper, path);
                  usingUiFrontend = true;
                } else {
                  throw fallbackStatError;
                }
              }
            }

            if (request.headers.Navigate) {
              gateway.setNavigationProgress(request.headers['Tab-Index'], 0.75);
            }

            newHyperInfo.browserContexts.push('files');
            const {join} = require('path');
            // TODO H - Replace with function's allowed TEMP_DIR
            const tempHyperDir = join(global.tmpDir, '/Hyper/', key);
            let fileDir = '/';
            let displayPath;
            if (context === 'app') {
              // TODO H - Handle Apps:
              //        - Determine root .js file (index(.os).js -> entry(.os).js -> main(.os).js -> app(.os).js -> (anything)(.os).js)
              //        - Switch to site if missing .js file
              //        - Write to FS if there's a version discrepency
              //        - Return results and end function
            }
            if (context === 'site') {
              if (stats.isDirectory()) {
                let pathContents = await pda.readdir(hyper, path, {
                  includeStats: true,
                });
                let foundUiFolder = pathContents.find(obj =>
                  obj.name === ('.ui'),
                );

                function getSiteContent(content) {
                  const htmlContents = content.filter(obj =>
                    obj.name.endsWith('.html'),
                  );
                  let siteContents = htmlContents;
                  let siteExtension = '.html';
                  if (htmlContents.length === 0) {
                    const mdContents = content.filter(obj =>
                      obj.name.endsWith('.md'),
                    );
                    if (mdContents.length === 0) {
                      context = 'files';
                    } else {
                      siteContents = mdContents;
                      siteExtension = '.md';
                    }
                  }
                  return {siteContents, siteExtension};
                }

                if (request.headers.Navigate && foundUiFolder === undefined && path !== '/') {
                  try {
                    const uiTest = await pda.stat(hyper, '/.ui', {
                      includeStats: true,
                    });
                    path = '/'
                    pathContents = await pda.readdir(hyper, path, {
                      includeStats: true,
                    });
                    foundUiFolder = true;
                  } catch (uiError) {console.log('[NODE] No /.ui folder')}
                }

                if (request.headers.Navigate && foundUiFolder !== undefined) {

                  // Get file path to display for input text
                  let {siteContents, siteExtension} = getSiteContent(pathContents);
                  if (siteContents.length > 0) {
                    const folderName = path.split('/').reverse()[0];
                    const indexFile = 'index' + siteExtension;
                    if (siteContents.find(x => x.name === indexFile)) {
                      const indexFilePath = '/' + indexFile;
                      displayPath = join(path, indexFilePath);
                    } else if (
                      siteContents.find(x => x.name === folderName + siteExtension)
                    ) {
                      displayPath = join(path, '/' + folderName + siteExtension);
                    } else {
                      const fileName = '/' + siteContents[0].name;
                      displayPath = join(path, fileName);
                    }
                  }

                  // To be consistent with Beaker behavior, /.ui should override frontend loading
                  path = (path + '/.ui').replace('//', '/');
                  pathContents = await pda.readdir(hyper, path, {
                    includeStats: true,
                  });
                }
                let {siteContents, siteExtension} = getSiteContent(pathContents);
                if (siteContents.length === 0) {
                  context = 'files';
                }
                if (context === 'site') {
                  // Load html from directory
                  newHyperInfo.browserContexts.push('site');
                  try {
                    var testAppContents = await pda.readdir(hyper, '/.rn').filter(obj => obj.indexOf('.js') !== -1);
                    if (testAppContents.length > 0) {
                      newHyperInfo.browserContexts.push('app');
                    }
                  } catch (appError) {
                    console.log('[NODE] No native app.');
                  }
                  const folderName = path.split('/').reverse()[0];
                  const indexFile = 'index' + siteExtension;
                  if (siteContents.find(x => x.name === indexFile)) {
                    const indexFilePath = '/' + indexFile;
                    console.log('[NODE] fileName 1:', indexFilePath);
                    path = join(path, indexFilePath);
                  } else if (
                    siteContents.find(x => x.name === folderName + siteExtension)
                  ) {
                    console.log(
                      '[NODE] fileName 2:',
                      '/' + folderName + siteExtension,
                    );
                    path = join(path, '/' + folderName + siteExtension);
                  } else {
                    const fileName = '/' + siteContents[0].name;
                    console.log('[NODE] fileName 3:', fileName);
                    path = join(path, fileName);
                  }
                  console.log('[NODE] Dat tempHyperDir:', tempHyperDir);
                  console.log('[NODE] Dat path:', path);
                  fileDir = join(tempHyperDir, path);
                  console.log('[NODE] Dat fileDir 1:', fileDir);
                  results.uri = fileDir;
                }
              } else {
                // Load file for WebView
                newHyperInfo.browserContexts.push('site');
                try {
                  var testAppContents = await pda.readdir(hyper, '/.rn').filter(obj => obj.indexOf('.js') !== -1);
                  if (testAppContents.length > 0) {
                    newHyperInfo.browserContexts.push('app');
                  }
                } catch (appError) {
                  console.log('[NODE] No native app.');
                }
                fileDir = join(tempHyperDir, path);
                console.log('[NODE] Dat fileDir 2:', fileDir);
                results.uri = fileDir;
              }
            }
            if (context !== 'site') {
              path = pathname;
            }
            if (context === 'files') {
              if (stats.isDirectory()) {
                const pathContents = await pda.readdir(hyper, path, {
                  includeStats: true,
                });
                results.contents = pathContents;
              } else {
                const fileContent = await pda.readFile(hyper, path, 'utf8');
                results.content = fileContent;
                results.stat = stats;
              }
            }
            results.context = context;
            results.inputUrl = versionUrl + path;
            console.log('[NODE] Hyper Nav Pre-FS Result:', results);
            if (results.uri) {
              let changes;
              const sourceExists = require('fs').existsSync(results.uri);
              if (request.headers['Site-Info'] && sourceExists) {
                const hyperInfo = request.headers['Site-Info'];
                if (hyperInfo.lastFsVersion) {
                  const lastFsVersion =
                    typeof hyperInfo.lastFsVersion === 'string'
                      ? Number(hyperInfo.lastFsVersion)
                      : hyperInfo.lastFsVersion;
                  changes = await pda.diff(hyper, lastFsVersion);
                }
              }
              console.log('[NODE] Hyper Nav Pre-FS sourceExists:', sourceExists);
              console.log('[NODE] Hyper Nav Pre-FS changes:', changes);
              if (!sourceExists || changes) {
                if (request.headers.Navigate) {
                  gateway.setNavigationProgress(
                    request.headers['Tab-Index'],
                    0.9,
                  );
                }

                const uriParentDir = results.uri.substr(
                  0,
                  results.uri.lastIndexOf('/'),
                );
                const uriParentDirExists = require('fs').existsSync(
                  uriParentDir,
                );
                if (!uriParentDirExists) {
                  require('fs').mkdirSync(uriParentDir, {recursive: true});
                }
                let ignore = [
                  '.dat',
                  '**/.dat',
                  '.git',
                  '**/.git',
                  '.mov',
                  '**/.mov',
                  '.mp4',
                  '**/.mp4',
                  '.m4v',
                  '**/.m4v',
                  '.webm',
                  '**/.webm',
                  '.wmv',
                  '**/.wmv',
                  '.flv',
                  '**/.flv',
                  '.avi',
                  '**/.avi',
                ];

                function endsWithAny(suffixes, string) {
                  return suffixes.some(function (suffix) {
                    return string.endsWith(suffix);
                  });
                }

                if (!request.headers.Navigate || endsWithAny(ignore, path)) {
                  ignore = undefined;
                }
                const fsStats = await EXPORT_TO_FILESYSTEM({
                  srcArchive: hyper,
                  dstPath: results.uri,
                  baseFsPath: tempHyperDir,
                  skipUndownloadedFiles: false,
                  overwriteExisting: true,
                  transformRelativePaths: true,
                  changes,
                  srcPath: path,
                  walkStart: true,
                  baseUrl,
                  treatLikeRoot: '/.ui',
                  ignore,
                });
                console.log('[NODE] Finished writing to filesystem:', fsStats);
                newHyperInfo.lastFsVersion = hyper.version;
              }
            }

            // Save hyper info
            gateway.setSiteInfo(request.url, newHyperInfo);

            // To be consistent with Beaker behavior, use of /.ui frontend shouldn't affect input url
            if (
              request.headers.Navigate &&
              context === 'site' &&
              results.uri &&
              results.uri.indexOf('/.ui') !== -1
            ) {
              if (displayPath) {
                results.inputUrl = versionUrl + displayPath;
              } else {
                results.inputUrl = typeof request.url === 'string' ? request.url : request.url.href;
              }
            }
            results.requestUrl = typeof request.url === 'string' ? request.url : request.url.href;

            console.log('[NODE] Next source:', results);
            yield {
              tabIndex: request.headers['Tab-Index'],
              source: results,
            };
          }
        }`,
        // For iOS/Android 'share' functionality
        SHARE: `async function (request) {
          console.log('[NODE] Share Hyper:', request);

          if (!request.headers) {
            request.headers = {};
          }

          // TODO H - Replace with PARSED_URL
          let {protocol, key, version, pathname} = await parseURL(request.url);

          console.log('[NODE] URL Parsed:', key, version, pathname);

          const fetchHyper = await fetch(request.url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyper',
              'Hyper-Options': request.headers['Hyper-Options'],
            },
          });
          let hyper = await fetchHyper.body.next();
          if (hyper.value) {
            hyper = hyper.value;
          }

          const {join} = require('path');
          // TODO H - Replace with function's allowed TEMP_DIR
          const filePath = join(global.tmpDir, '/Hyper/', key, pathname);
          const fileExists = require('fs').existsSync(filePath);

          // Check if file is already written to filesystem
          if (fileExists) {
            if (request.headers['Site-Info']) {
              const hyperInfo = request.headers['Site-Info'];
              if (hyperInfo.lastFsVersion) {
                const lastFsVersion = typeof hyperInfo.lastFsVersion === 'string' ? Number(hyperInfo.lastFsVersion) : hyperInfo.lastFsVersion;
                if (lastFsVersion === hyper.version) {
                  // Correct file version is already written
                  console.log('[NODE] Hyperdrive file already in fs!', filePath);
                  return filePath;
                }
              }
            }
          }

          // Ensure file parent directory exists
          const fileParentDir = filePath.substring(0, filePath.lastIndexOf('/'));
          const parentDirExists = require('fs').existsSync(fileParentDir);
          console.log('Ensuring path...');
          if (!parentDirExists) {
            require('fs').mkdirSync(fileParentDir, {recursive: true});
          }
          console.log('Path ensured!', fileParentDir);

          // Write file to filesystem
          const fsStats = await EXPORT_TO_FILESYSTEM({
            srcArchive: hyper,
            dstPath: filePath,
            srcPath: pathname,
            skipUndownloadedFiles: false,
            overwriteExisting: true,
            transformRelativePaths: true,
          });

          console.log('[NODE] Finished writing file to fs!', filePath);
          return filePath;
        }`,
      },
      // [Optional] Different contexts that can be switched to interpret
      browserContexts: [
        // {name: 'app', icon: APP_SVG},
        {name: 'site', icon: SITE_SVG},
        {name: 'files', icon: FILES_SVG},
        {name: 'feed', icon: SOURCE_SVG},
      ],
      // [Optional] - The url property that each instance of the protocol uses
      //              for its path in the filesystem.
      //            - Default: hostname
      instanceId: 'key',
    },
  ]),
  {
    ['REGISTER_PROTOCOL'](state, action) {},
  },
);

export const apis = createReducer(
  Map({
    gateway: {
      // TODO H - Call gateway.setSiteInfo, but use CURRENT_URL if no url is passed
      setSiteInfo: ``,
      markdownToHtml: 'gateway.markdownToHtml',
    },
    // TODO H - Finish Beaker API
    // TODO H - Add Beaker's unique functions to hyperdrives
    //        - IDEA: Proxy all hyperdrives returned by beaker.drive/createDrive/forkDrive that catches any missing functions and assumes it needs to use the beaker api
    //        -     : This would mean the api functions would need to be aware of what apis are availabe, or at least their own api (could pass API constant that refers to the main parent, in this case 'beaker')
    beaker: {
      capabilities: {
        create: ``,
        modify: ``,
        delete: ``,
      },
      contacts: {
        // TODO H - Develop Gateway User/Contacts API and then Beaker shim
        requestProfile: ``,
        requestContact: ``,
        requestContacts: ``,
        list: ``,
      },
      hyperdrive: {
        drive: `function(url) {
          return Hyper.Hyperdrive(url);
        }`,
        createDrive: ``,
        forkDrive: ``,
        getInfo: `async function(url = CURRENT_URL) {
          const driveFetch = await fetch(url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();
          const manifest = await pda.readManifest(drive);
          return {
            key: drive.key.toString('hex'),
            url: 'hyper://' + drive.key.toString('hex') + '/',
            writable: drive.writable,
            version: drive.version,
            title: manifest.title,
            description: manifest.description,
          };
        }`,
        stat: `async function(url, opts = {}) {
          if (url.indexOf('hyper://') === -1) {
            if (url === '') {
              url = '/';
            }
            const {hostname} = basicParseURL(CURRENT_URL);
            url = 'hyper://' + hostname + url;
          }
          const driveFetch = await fetch(url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();
          const {pathname} = basicParseURL(url);
          const stat = await pda.stat(drive, pathname, opts);
          return stat;
        }`,
        readFile: `async function(url, opts = {}) {
          if (url.indexOf('hyper://') === -1) {
            if (url === '') {
              url = '/';
            }
            const {hostname} = basicParseURL(CURRENT_URL);
            url = 'hyper://' + hostname + url;
          }
          const driveFetch = await fetch(url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();
          const {pathname} = basicParseURL(url);
          const encoding = opts.encoding || 'utf8';
          const file = await drive.readFile(pathname, {encoding});
          return file;
        }`,
        readdir: `async function(url, opts = {}) {
          if (url.indexOf('hyper://') === -1) {
            if (url === '') {
              url = '/';
            }
            const {hostname} = basicParseURL(CURRENT_URL);
            url = 'hyper://' + hostname + url;
          }
          const driveFetch = await fetch(url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();
          const {pathname} = basicParseURL(url);
          const includeStats = opts.includeStats || false;
          const recursive = opts.recursive || false;
          const contents = await drive.readdir(pathname, {includeStats, recursive});
          return contents;
        }`,
        // TODO H - Write to fs if this request is being done from the WebView and the file doesn't exist yet
        query: `async function(query) {
          let {
            drive,
            path,
            // TODO H - Apply these in fetch
            sort,
            type,
            mount,
            limit,
            offset,
            reverse,
            metadata,
            timeout,
          } = query;
          let {hostname, pathname} = basicParseURL(CURRENT_URL);
          if (!drive) {
            const url = 'hyper://' + hostname;
            drive = url;
          }
          if (!path) {
            path = '/';
          }
          const drives = typeof drive === 'string' ? [drive] : drive;
          let paths = typeof path === 'string' ? [path] : path;
          if (CURRENT_LOCAL_PATH) {
            paths = paths.map(path => {
              const splitPath = path.split('/');
              let localFsPath = CURRENT_LOCAL_PATH;
              const uiIndex = localFsPath.indexOf('/.ui');
              if (uiIndex !== -1) {
                if (!pathname || pathname === '') {
                  pathname = '/';
                }
                localFsPath = localFsPath.substring(0, uiIndex) + pathname;
              }
              const splitLocal = localFsPath.split('/');
              let relativePath = [];
              for (var i = 0; i < splitPath.length; i++) {
                let subPath = splitPath[i];
                if (splitLocal[i] !== subPath) {
                  relativePath.push(subPath);
                }
              }
              relativePath = relativePath.join('/');
              if (relativePath.indexOf('/') !== 0) {
                relativePath = '/' + relativePath;
              }
              return relativePath;
            });
          }
          let requests = [];
          for (var i = 0; i < drives.length; i++) {
            let urlBase = drives[i];
            for (var j = 0; j < paths.length; j++) {
              let pathname = paths[j];
              if (pathname === '') {
                pathname = '/';
              }
              let fullUrl =
                !urlBase.endsWith('/') && !pathname.startsWith('/')
                  ? urlBase + '/' + pathname
                  : urlBase.endsWith('/') && pathname.startsWith('/')
                  ? urlBase + pathname.replace('/', '')
                  : urlBase + pathname;
              requests.push(fullUrl);
            }
          }
          console.log('[NODE] Beaker Query Requests:', requests);
          const fetchRequest = await fetch(requests, {
            method: 'QUERY',
            headers: {
              // 'Conntent-Type' should conform to expected fetch API behavior, so Beaker's 'type' (file, folder, mount) should be different
              'Child-Type': type,
              'Mount-URL': mount,
              Metadata: metadata,
              Reverse: reverse,
              Offset: offset,
              Limit: limit,
              Sort: sort,
            },
          });
          console.log('[NODE] Query got initial fetch response');
          let fetchResult = await fetchRequest.body.next();
          console.log('[NODE] Query body:', fetchResult);
          if (fetchResult.value) {
            fetchResult = fetchResult.value;
          }
          let queryResults = [];
          if (!Array.isArray(fetchResult)) {
            fetchResult = [fetchResult];
          }
          fetchResult = fetchResult.filter(res => {
            if (!res.status || res.status === 'fulfilled') {
              return true;
            }
            return false;
          });
          fetchResult.map(res => {
            let value = res.value || res;
            if (Array.isArray(value)) {
              queryResults = queryResults.concat(value);
            } else {
              queryResults.push(value);
            }
          });
          console.log('[NODE] About to sort query results');
          // HACK - resort and slice here because each query was run separately
          if (sort === 'name') {
            queryResults.sort((a, b) =>
              reverse
                ? require('path')
                    .basename(b.path)
                    .toLowerCase()
                    .localeCompare(
                      require('path')
                        .basename(a.path)
                        .toLowerCase(),
                    )
                : require('path')
                    .basename(a.path)
                    .toLowerCase()
                    .localeCompare(
                      require('path')
                        .basename(b.path)
                        .toLowerCase(),
                    ),
            );
          } else if (sort === 'mtime') {
            queryResults.sort((a, b) =>
              reverse
                ? b.stat.mtime - a.stat.mtime
                : a.stat.mtime - b.stat.mtime,
            );
          } else if (sort === 'ctime') {
            queryResults.sort((a, b) =>
              reverse
                ? b.stat.ctime - a.stat.ctime
                : a.stat.ctime - b.stat.ctime,
            );
          }
          if (offset && limit) {
            queryResults = queryResults.slice(offset, offset + limit);
          } else if (offset) {
            queryResults = queryResults.slice(offset);
          } else if (limit) {
            queryResults = queryResults.slice(0, limit);
          }
          console.log('[NODE] Finished query:', queryResults);
          return queryResults;
        }`,
        // TODO H - Support passed hyperdrive from webview (if object, get hyperdrive corresponding to the _bridgeId)
        diff: `async function(url, other, opts = {}) {
          const driveFetch = await fetch(url, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          const diffDriveFetch =
            typeof other === 'string'
              ? await fetch(other, {
                  method: 'GET',
                  headers: {
                    'Content-Type': 'application/hyperdrive',
                  },
                })
              : undefined;
          const diffDrive = diffDriveFetch
            ? await diffDriveFetch.body.next()
            : other;
          if (diffDriveFetch && diffDrive.value) {
            diffDrive = diffDrive.value;
          }
          const difference = await pda.diff(drive, diffDrive);
          return difference;
        }`,
        // TODO H - TEST API SO FAR
        configure: ``,
        writeFile: ``,
        mkdir: ``,
        symlink: ``,
        mount: ``,
        copy: ``,
        rename: ``,
        updateMetadata: ``,
        unlink: ``,
        rmdir: ``,
        unmount: ``,
        deleteMetadata: ``,
      },
      markdown: {
        toHTML: 'gateway.markdownToHtml',
      },
      peersockets: {
        join: `async function(topic) {
          const driveFetch = await fetch(CURRENT_URL, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();
          topic = \`webapp/\${drive.discoveryKey.toString('hex')}/\${topic}\`;

          var topicHandle = Hyper.peersockets.join(topic, {
            onmessage(peerId, message) {
              stream.push(['message', {peerId, message}]);
            },
          });

          const {Duplex} = streamx;
          var stream = new Duplex({
            write(data, cb) {
              if (
                !Array.isArray(data) ||
                typeof data[0] === 'undefined' ||
                typeof data[1] === 'undefined'
              ) {
                return cb(null);
              }
              topicHandle.send(data[0], data[1]);
              cb(null);
            },
          });
          stream.objectMode = true;
          stream.on('close', () => {
            topicHandle.close();
          });

          var obj = global.fromEventStream(stream);
          obj.send = (peerId, msg) => {
            stream.write([peerId, msg]);
          }
          return obj;
        }`,
        watch: `async function() {
          const driveFetch = await fetch(CURRENT_URL, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/hyperdrive',
            },
          });
          let drive = await driveFetch.body.next();
          if (drive.value) {
            drive = drive.value;
          }
          await drive.ready();

          const {Readable} = streamx;
          var stream = new Readable();
          var stopwatch = Hyper.peersockets.watchPeers(drive.discoveryKey, {
            onjoin: async peerId => stream.push(['join', {peerId}]),
            onleave: peerId => stream.push(['leave', {peerId}]),
          });
          stream.on('close', () => stopwatch());

          return global.fromEventStream(stream);
        }`,
      },
      // TODO H - beaker.index, beaker.sessions, beaker.subscriptions
      //        - Found at https://github.com/beakerbrowser/beaker/tree/master/app/bg/web-apis/bg
      index: {
        clearAllData: ``,
        getSite: ``,
        listSites: ``,
        getRecord: ``,
        listRecords: ``,
        countRecords: ``,
        searchRecords: ``,
        clearNotifications: ``,
        getState: ``,
        createEventStream: ``,
      },
      sessions: {
        request: ``,
        get: ``,
        destroy: ``,
      },
      subscriptions: {
        list: ``,
        listNetworkFor: ``,
        get: ``,
        add: ``,
        remove: ``,
      },
      // Missing: beaker.shell or beaker.terminal
    },
  }),
  {
    ['REGISTER_API'](state, action) {},
  },
);

export const siteInfo = createReducer(Map({}), {
  ['SET_SITE_INFO'](state, action) {
    const infoKey = getInfoKey(action.url);
    let infoEntry = state.get(infoKey);
    if (!infoEntry) {
      // Create info entry
      let meta = {};
      for (const prop in action.info) {
        meta[prop] = action.timestamp;
      }
      infoEntry = {
        info: action.info,
        _meta: meta,
      };
      return state.set(infoKey, infoEntry);
    } else {
      // Merge based on timestamp vs meta for attributes
      for (const prop in action.info) {
        const lastTimestamp = infoEntry._meta[prop];
        if (
          !action.timestamp ||
          !lastTimestamp ||
          lastTimestamp < action.timestamp
        ) {
          infoEntry._meta[prop] = action.timestamp;
          if (prop === 'display') {
            let previousDisplay = infoEntry.info[prop] || [];
            previousDisplay = previousDisplay.filter(el => {
              const elKey = typeof el === 'object' ? el.key : el;
              const testArray = action.info[prop].filter(testEl => {
                const testElKey =
                  typeof testEl === 'object' ? testEl.key : testEl;
                if (testElKey === elKey) {
                  return true;
                }
                return false;
              });
              if (testArray.length > 0) {
                return false;
              }
              return true;
            });
            infoEntry.info[prop] = action.info[prop].concat(previousDisplay);
          } else {
            infoEntry.info[prop] = action.info[prop];
          }
        }
      }
      return state.set(infoKey, infoEntry);
    }
  },
});
