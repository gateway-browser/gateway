import {Dimensions} from 'react-native';
import {createReducer} from 'redux-immutablejs';
const {Map, List} = require('immutable');

const {height: screenHeight, width: screenWidth} = Dimensions.get('window');

// TODO M - Aesthetic customization
// TODO L - Setting custom themes for specific domains/pages
export const theme = createReducer(
  Map({
    chameleonMode: true,
    default: Map({
      backgroundColor: 'rgba(0,0,0,1.0)',
      textColor: 'rgba(255,255,255,1.0)',
      highlightColor: 'rgba(0,227,0,1.0)',
    }),
    chameleon: Map({
      backgroundColor: 'rgba(0,0,0,1.0)',
      textColor: 'rgba(255,255,255,1.0)',
      highlightColor: 'rgba(0,227,0,1.0)',
    }),
    // Entries should be like above default, only defining the exceptions
    custom: Map({}),
  }),
  {
    ['SET_THEME_COLOR'](state, action) {
      let subState = state.get('default');
      if (action.url) {
        subState = state.get('custom');
        subState = subState.get(action.url);
      }
      if (!subState) {
        subState = Map({});
      }
      const newSubState = subState.set(action.category, action.color);
      if (action.url) {
        const newState = state.get('custom').set(action.url, newSubState);
        return state.set('custom', newState);
      }
      return state.set('default', newSubState);
    },
    ['SET_THEME_CHAMELEON'](state, action) {
      return state.set('chameleon', Map(action.theme));
    },
    ['TOGGLE_THEME_CHAMELEON_MODE'](state, action) {
      if (!action.url) {
        let currentChameleon = state.get('chameleonMode');
        return state.set('chameleonMode', !currentChameleon);
      }
      let subState = state.get('custom').get(action.url);
      if (!subState) {
        subState = Map({});
      }
      let currentChameleon = subState.get('chameleonMode');
      if (currentChameleon === undefined || currentChameleon === null) {
        currentChameleon = state.get('chameleonMode');
      }
      const newSubState = subState.set('chameleonMode', !currentChameleon);
      const newState = state.get('custom').set(action.url, newSubState);
      return state.set('custom', newState);
    },
    ['RESET_CHAMELEON_THEME'](state, action) {
      const defaultTheme = state.get('default');
      return state.set('chameleon', defaultTheme);
    },
  },
);

export const navigationMeasurements = createReducer(
  Map({topBarHeight: 0, bottomBarHeight: 0}),
  {
    ['SET_TOP_BAR_HEIGHT'](state, action) {
      return state.set('topBarHeight', action.height);
    },
    ['SET_BOTTOM_BAR_HEIGHT'](state, action) {
      return state.set('bottomBarHeight', action.height);
    },
  },
);

export const navButtonPosition = createReducer(
  Map({
    fwdX: 0,
    fwdY: 0,
    backX: 0,
    backY: screenHeight - 5,
    omniX: 0,
    omniY: 0,
    optX: 0,
    optY: 0,
  }),
  {
    ['SET_FWD_BUTTON_POSITION'](state, action) {
      const newVals = Map({fwdX: action.x, fwdY: action.y});
      return state.merge(newVals);
    },
    ['SET_BACK_BUTTON_POSITION'](state, action) {
      const newVals = Map({backX: action.x, backY: action.y});
      return state.merge(newVals);
    },
    ['SET_OMNI_BUTTON_POSITION'](state, action) {
      const newVals = Map({omniX: action.x, omniY: action.y});
      return state.merge(newVals);
    },
    ['SET_OPT_BUTTON_POSITION'](state, action) {
      const newVals = Map({optX: action.x, optY: action.y});
      return state.merge(newVals);
    },
  },
);

export const deviceDimensions = createReducer(
  Map({orientation: 'portrait', width: screenWidth, height: screenHeight}),
  {
    ['SET_DEVICE_DIMENSIONS'](state, action) {
      const newVals = Map({
        orientation: action.orientation,
        width: action.width,
        height: action.height,
      });
      return state.merge(newVals);
    },
  },
);

export const toast = createReducer(Map({request: 0, message: ''}), {
  ['SET_TOAST_MESSAGE'](state, action) {
    const newState = Map({
      message: action.message,
      request: Math.random(),
    });
    return newState;
  },
});

export const setup = createReducer(Map({browser: false}), {
  ['START_BROWSER'](state, action) {
    return state.set('browser', true);
  },
});
