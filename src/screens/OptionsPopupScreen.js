/**
 * @format
 * @flow
 */

import React from 'react';

import PopupModalBackground from '../views/PopupModalBackground';
import OptionsPopup from '../views/OptionsPopup';

const OptionsPopupScreen = ({
  route: {
    params: {tabIndex},
  },
}: props) => {
  return (
    <PopupModalBackground button={'opt'}>
      <OptionsPopup tabIndex={tabIndex} />
    </PopupModalBackground>
  );
};

export default OptionsPopupScreen;
