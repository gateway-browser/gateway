/**
 * @format
 * @flow
 */

import React, {Component, Fragment} from 'react';
import {Platform} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import TopBrowserBar from '../views/TopBrowserBar';
import BottomBrowserBar from '../views/BottomBrowserBar';

import PageScreen from './PageScreen';

const Stack = createStackNavigator();

class BrowserTabScreen extends Component {
  constructor() {
    super();
  }
  render() {
    const {
      props: {
        route: {params},
      },
    } = this;
    const tabIndex = params ? params.index : 0;
    // TODO L - Distributed Native Apps (host them and fork them on DAT)
    // TODO L - Native screens for specific filetypes (.pdf)
    return (
      <Fragment>
        <Stack.Navigator
          initialRouteName="browser"
          keyboardHandlingEnabled={false}
          gestureEnabled={true}
          headerMode="float"
          screenOptions={{
            headerTransparent: true,
            header: props => <TopBrowserBar {...props} tabIndex={tabIndex} />,
          }}>
          <Stack.Screen name="browser">
            {props => <PageScreen {...props} tabIndex={tabIndex} />}
          </Stack.Screen>
        </Stack.Navigator>
        {Platform.OS === 'ios' ? (
          <BottomBrowserBar tabIndex={tabIndex} />
        ) : null}
      </Fragment>
    );
  }
}

export default BrowserTabScreen;
