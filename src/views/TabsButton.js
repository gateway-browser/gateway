/**
 * @format
 * @flow
 */

import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {systemWeights} from 'react-native-typography';
import {connect} from 'react-redux';

const TabsButton = ({tabIndex, theme, tabHistory}: props) => {
  const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
  const textColor = theme.get(subState).get('textColor');
  const tabCount = tabHistory.get(tabIndex).history.size;
  const tabText = tabCount < 100 ? `${tabCount}` : ':)';
  const onPress = () => {
    // TODO L - navigate to tab selection
  };
  const onLongPress = () => {
    // TODO L - bring up tab options (New Tab, New Incognito Tab, Close This Tab, Close All Tabs)
  };
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={onPress}
      onLongPress={onLongPress}>
      <View style={[styles.box, {borderColor: textColor}]}>
        <Text
          style={[styles.text, {color: textColor}]}
          allowFontScaling={false}>
          {tabText}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    ...systemWeights.regular,
    fontSize: 14,
  },
  box: {
    height: 24,
    width: 24,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 5,
  },
});

function mapStateToProps(state) {
  return {
    theme: state.theme,
    tabHistory: state.tabHistory,
  };
}

export default connect(mapStateToProps)(TabsButton);
