/**
 * @format
 * @flow
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import PopupModalWindow from '../views/PopupModalWindow';
import PopupModalList from '../views/PopupModalList';
import PopupModalListItem from '../views/PopupModalListItem';
import NavigationService from '../lib/NavigationService';
import {ActionCreators} from '../actions';

const TabHistoryPopup = ({
  tabIndex,
  isBack,
  navBack,
  navForward,
  tabHistory,
}: props) => {
  const screenRatio = 4 / 5;
  const {currentIndex, history} = tabHistory.get(tabIndex);
  let listData = [];
  if (isBack) {
    const spliceNumber = currentIndex + 1;
    listData = history.splice(0, spliceNumber);
  } else {
    const spliceStart = currentIndex;
    const diff = history.size - spliceStart;
    listData = history.splice(spliceStart, diff).reverse();
  }

  const renderItem = ({item, index}) => {
    const maxIndex = listData.size - 1;
    const listItemPressed = () => {
      const iterations = index + 1;
      isBack ? navBack(tabIndex, iterations) : navForward(tabIndex, iterations);
      NavigationService.goBack();
    };
    const title = item.inputUrl || item.uri || item;
    return (
      <PopupModalListItem
        index={index}
        maxIndex={maxIndex}
        title={title}
        onPress={listItemPressed}
        numberOfLines={1}
      />
    );
  };

  const keyExtractor = (item, index) => {
    const url = item.inputUrl || item.uri || item;
    return url + index;
  };

  return (
    <PopupModalWindow screenRatio={screenRatio}>
      <PopupModalList
        screenRatio={screenRatio}
        immutableData={listData}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </PopupModalWindow>
  );
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabHistory: state.tabHistory,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TabHistoryPopup);
