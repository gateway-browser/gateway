/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {LayoutAnimation, Platform, UIManager, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeView from './ThemeView';
import {ActionCreators} from '../actions';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}
const PopupModalWindow = ({
  children,
  screenRatio,
  deviceDimensions,
  omniButton,
  ...props
}: props) => {
  const screenWidth = deviceDimensions.get('width');
  const [windowSize, setWindowSize] = useState({width: 0, height: 0});
  useEffect(() => {
    const windowWidth = screenRatio * screenWidth;
    LayoutAnimation.configureNext(
      LayoutAnimation.create(
        100,
        'easeInEaseOut',
        LayoutAnimation.Properties.scaleXY,
      ),
    );
    setWindowSize({width: windowWidth, height: -1});
  }, [screenRatio, screenWidth]);
  let sizeStyle = {
    height: windowSize.height,
    width: windowSize.width,
  };
  return (
    <ThemeView
      {...props}
      style={[styles.window, sizeStyle]}
      shadowColor="rgba(0,0,0,0.2)"
      shadowOffset={{width: 0, height: 0}}
      shadowOpacity={1}
      shadowRadius={10}>
      {children}
    </ThemeView>
  );
};

const styles = StyleSheet.create({
  window: {
    overflow: 'hidden',
    borderRadius: 15,
    marginHorizontal: 5,
    marginVertical: 40,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopupModalWindow);
