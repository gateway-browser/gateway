/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {ImmutableVirtualizedList} from 'react-native-immutable-list-view';

import {ActionCreators} from '../actions';

const PopupModalList = ({screenRatio, deviceDimensions, ...props}: props) => {
  const [listContentHeight, setContentHeight] = useState(0);
  const screenHeight = deviceDimensions.get('height');
  const screenWidth = deviceDimensions.get('width');
  const onContentSizeChange = (contentWidth, contentHeight) => {
    setContentHeight(contentHeight);
  };
  const maxHeight = screenHeight * 0.6;
  const height = Math.min(listContentHeight, maxHeight);
  const width = screenRatio * screenWidth;
  const layoutStyle = {
    height,
    width,
  };
  return (
    <ImmutableVirtualizedList
      {...props}
      onContentSizeChange={onContentSizeChange}
      style={[styles.list, layoutStyle]}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    backgroundColor: 'transparent',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopupModalList);
