/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import ThemeView from './ThemeView';

const LoadingBar = ({tabIndex, loading, navigationMeasurements}: props) => {
  const [containerWidth, setContainerWidth] = useState(0);

  const onLayout = ({
    nativeEvent: {
      layout: {width},
    },
  }) => {
    setContainerWidth(width);
  };

  const loadingProgress = loading.get(tabIndex);
  const isLoading = loadingProgress < 1;
  const indicatorWidth = containerWidth * loadingProgress;
  const top = navigationMeasurements.get('topBarHeight');

  if (!isLoading) {
    return null;
  }
  // TODO L - Show live progress of DAT loading
  return (
    <ThemeView style={[styles.container, {top}]} onLayout={onLayout}>
      <ThemeView type="highlightColor" style={styles.indicator} opacity={0.4}>
        <ThemeView
          type="highlightColor"
          style={[styles.indicator, {width: indicatorWidth}]}
        />
      </ThemeView>
    </ThemeView>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    backgroundColor: 'white',
    width: '100%',
    height: 4,
  },
  indicator: {
    width: '100%',
    height: 4,
  },
});

function mapStateToProps(state) {
  return {
    navigationMeasurements: state.navigationMeasurements,
    loading: state.loading,
  };
}

export default connect(mapStateToProps)(LoadingBar);
