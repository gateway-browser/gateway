/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import {PERMISSIONS, RESULTS, check} from 'react-native-permissions';
import SvgRenderer from 'react-native-svg-renderer';
import ImagePicker from 'react-native-image-crop-picker';
import CameraRoll from '@react-native-community/cameraroll';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {ActionCreators} from '../actions';
import {decodeQRImage} from '../lib/common';
import {photosSvg} from '../svg';

const CameraRollButton = ({scanQR}: props) => {
  const [lastImage, setLastImage] = useState(undefined);
  check(
    Platform.select({
      android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
    }),
  ).then(result => {
    if (result === RESULTS.GRANTED) {
      CameraRoll.getPhotos({
        first: 1,
        groupTypes: 'SavedPhotos',
        assetType: 'Photos',
      }).then(({edges}) => {
        if (edges.length > 0) {
          const imageUri = edges[0].node.image.uri;
          setLastImage(imageUri);
        }
      });
    }
  });
  const openImagePicker = () => {
    ImagePicker.openPicker({
      cropping: true,
      mediaType: 'photo',
      smartAlbums: [
        'Generic',
        'RecentlyAdded',
        'PhotoStream',
        'Screenshots',
        'Favorites',
        'Panoramas',
        'AllHidden',
        'Bursts',
        'UserLibrary',
        'SelfPortraits',
        'DepthEffect',
        'LivePhotos',
        'Animated',
        'LongExposure',
      ],
    })
      .then(image => {
        // TODO M - ANDROID: Decoding cropped QR image is less reliable
        decodeQRImage({
          path: image.path,
          width: image.width,
          height: image.height,
        })
          .then(result => {
            scanQR({data: result}, true);
          })
          .catch(qrErr => {
            Alert.alert('QR Error', 'Could not find a QR code in this image');
          });
      })
      .catch(err => {
        // Cancelled image picker
      });
  };
  const borderRadius = lastImage ? 11 : 22;
  return (
    <TouchableOpacity onPress={openImagePicker}>
      <View style={[styles.button, {borderRadius}]}>
        {lastImage ? (
          <Image
            source={{uri: lastImage}}
            style={[styles.image, {borderRadius}]}
          />
        ) : (
          <SvgRenderer
            height="17"
            width="20"
            fill="white"
            svgXmlData={photosSvg}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: 44,
    height: 44,
    margin: 20,
    marginBottom: 30,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  image: {
    width: 44,
    height: 44,
    overflow: 'hidden',
    backgroundColor: 'black',
  },
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CameraRollButton);
