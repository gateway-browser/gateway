const rn_bridge = require('rn-bridge');
const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const fetch = require('node-fetch');

// [BABEL IS NO LONGER NEEDED]
// const writeDocumentsModules = require('./modules-to-documents.js');

const markdownToHtml = require('./markdown/api.js');
const {formatError, makeFsExport} = require('./common.js');
const {
  EventTarget,
  fromEventStream,
  fromAsyncEventStream,
} = require('./event-target.js');
const {
  fetchHandler,
  fetchCallback,
  functionFromString,
} = require('./generators.js');

const setStrongProp = (target, prop, value) => {
  if (typeof value === 'object') {
    value = Object.freeze(value);
  }
  Object.defineProperty(target, prop, {
    value,
    writable: false,
    enumerable: false,
    configurable: false,
  });
};
setStrongProp(global, 'fromEventStream', fromEventStream);
setStrongProp(global, 'fromAsyncEventStream', fromAsyncEventStream);
setStrongProp(global, 'Peersockets', require('peersockets'));
setStrongProp(global, 'basicParseURL', require('url-parse'));
setStrongProp(global, 'pda', require('pauls-dat-api2'));
setStrongProp(global, 'hexTo32', require('hex-to-32'));
setStrongProp(global, 'streamx', require('streamx'));
setStrongProp(global, 'DatSDK', require('dat-sdk'));
var lastSiteInfoUpdate = new Map();
const bridgeResults = {};
var bridgeEmitters = [];

const getType = item => {
  if (item === undefined) {
    return 'undefined';
  }
  if (item === null) {
    return 'null';
  }
  if (typeof item === 'function') {
    if (/^class\s/.test(Function.prototype.toString.call(item))) {
      return 'class';
    }
    if (/^async\s/.test(Function.prototype.toString.call(item))) {
      return 'async function';
    }
    return 'function';
  }
  if (Array.isArray(item)) {
    return 'array';
  }
  if (Buffer.isBuffer(item)) {
    return 'buffer';
  }
  if (
    item instanceof require('events').EventEmitter ||
    item instanceof EventTarget
  ) {
    // TODO H - Store emitters for running page(s) so they can be closed when navigating away
    return 'emitter';
  }
  if (typeof item === 'object' && item.then) {
    return 'promise';
  }
  if (
    typeof item === 'object' &&
    item.getMonth &&
    typeof item.getMonth === 'function'
  ) {
    return 'date';
  }
  if (
    typeof item === 'object' &&
    item.constructor &&
    item.constructor.constructor &&
    item.constructor.constructor.name === 'AsyncGeneratorFunction'
  ) {
    return 'generator';
  }
  if (
    typeof item === 'object' &&
    item.constructor &&
    item.constructor.constructor &&
    item.constructor.constructor.name === 'AsyncGeneratorFunction'
  ) {
    return 'async generator';
  }
  return typeof item;
};

// TODO H - Add Support: Symbol, BigInt, RegExp, Map, Set, WeakMap, WeakSet, JSON
const bridgify = (result, requestId, layer = 0) => {
  if (requestId && layer === 0) {
    bridgeResults[requestId] = result;
  }
  const type = getType(result);
  if (type === 'class') {
    return `[CLASS]${requestId}`;
  }
  if (type === 'function') {
    return `[FUNCTION]${requestId}`;
  }
  if (type === 'async function') {
    return `[ASYNC_FUNCTION]${requestId}`;
  }
  if (type === 'generator') {
    return `[GENERATOR]${requestId}`;
  }
  if (type === 'async generator') {
    return `[ASYNC_GENERATOR]${requestId}`;
  }
  if (type === 'promise') {
    return `[PROMISE]${requestId}`;
  }
  if (type === 'emitter') {
    bridgeEmitters.push(requestId);
    return `[EMITTER]${requestId}`;
  }
  if (type === 'buffer') {
    return '[BUFFER]' + result.toString('base64');
  }
  if (type === 'date') {
    return '[DATE]' + result.getTime();
  }
  if (type === 'array') {
    // Go through array and see if anything inside needs to be modified
    let arrayResult = result.map((item, index) => {
      return bridgify(item, `${requestId}*${index}`, layer + 1);
    });
    return arrayResult;
  }
  if (type === 'object') {
    // Go through object and see if anything inside needs to be modified
    if (layer > 3) {
      return `[OBJECT]${requestId}`;
    }
    let objResult = {};
    for (const key in result) {
      objResult[key] = bridgify(result[key], `${requestId}*${key}`, layer + 1);
    }
    return objResult;
  }
  return result;
};

const proxyEmitter = (requestId, bridgeId, bridgeSource, action) => {
  console.log(
    '[NODE] Proxy emitter action:',
    action,
    ' for requestId:',
    requestId,
    'and bridgeId:',
    bridgeId,
    'on bridgeSource:',
    bridgeSource,
  );
  let method = action.type;
  if (method === 'addEventListener' && !bridgeSource[method]) {
    method = 'addListener';
  }
  if (method === 'removeEventListener' && !bridgeSource[method]) {
    method = 'removeListener';
  }
  bridgeSource[method](action.eventName, (...args) => {
    console.log('[NODE] Proxy emitter callback args:', [...args]);
    const bridgeResult = bridgify([...args], requestId);
    rn_bridge.channel.post(requestId, {
      eventName: action.eventName,
      callbackArgs: bridgeResult,
      bridgeId,
    });
  });
};
const proxyAction = async (source, action) => {
  const {type} = action;
  if (type === 'apply') {
    const {args} = action;
    const result = await source(...args);
    return result;
  }
  if (type === 'construct') {
    const {args} = action;
    const result = new source(...args);
    return result;
  }
  if (type === 'get') {
    if (source === undefined || source === null) {
      return source;
    }
    if (action.key === 'toJSON' && !source[action.key]) {
      return JSON.stringify(source);
    }
    return source[action.key];
  }
  if (type === 'defineProperty') {
    return Object.defineProperty(source, action.key, action.descriptor);
  }
  if (type === 'deleteProperty') {
    return delete source[action.key];
  }
  if (type === 'getOwnPropertyDescriptor') {
    return Object.getOwnPropertyDescriptor(source, action.key);
  }
  if (type === 'getPrototypeOf') {
    return Object.getPrototypeOf(source);
  }
  if (type === 'has') {
    return action.key in source;
  }
  if (type === 'isExtensible') {
    return Object.isExtensible(source);
  }
  if (type === 'preventExtensions') {
    return Reflect.preventExtensions(source);
  }
  if (type === 'promise') {
    try {
      const promiseResult = await source;
      return promiseResult;
    } catch (promiseError) {
      // TODO H - Complete error handling
      return promiseError;
    }
  }
  if (type === 'ownKeys') {
    return Reflect.ownKeys(source);
  }
  if (type === 'setPrototypeOf') {
    return Object.setPrototypeOf(source, action.prototype);
  }
  if (type === 'set') {
    return Reflect.set(source, action.key, action.value);
  }
  return source;
};

async function setup({apis, protocols, settings, listeners}) {
  console.log('[NODE] Starting setup...', apis, protocols, settings, listeners);
  if (!global.gateway) {
    try {
      // Setup gateway api
      setStrongProp(global, 'gateway', {
        setNavigationProgress: (tabIndex, progress) => {
          if (progress === undefined || progress === null) {
            progress = tabIndex;
            tabIndex = undefined;
          }
          rn_bridge.channel.post(listeners['navigation-progress'], {
            tabIndex,
            progress,
          });
        },
        // TODO H - Only setSiteInfo when the user actually needs it
        //        - POSSIBLE SOLUTION: Have a 'currentlyViewing' array (or Set?) that indicates what's being viewed on the other side of the bridge
        //        -                    Hyper's function would setSiteInfo if the url's hostname is in the 'currentlyViewing' list. If it fails, cancel the listeners
        setSiteInfo: (url, info, timeSensitive, continuous) => {
          const {hostname} = global.basicParseURL(url);
          const timestamp = new Date().getTime();
          const BUFFER_TIME_MS = 30000;
          if (
            !continuous ||
            !lastSiteInfoUpdate.has(hostname) ||
            timestamp - lastSiteInfoUpdate.get(hostname) >= BUFFER_TIME_MS
          ) {
            lastSiteInfoUpdate.set(hostname, timestamp);
            console.log('[NODE] Set Site Info', {
              url,
              info,
              timestamp: timeSensitive ? timestamp : undefined,
            });
            rn_bridge.channel.post(listeners['set-site-info'], {
              url,
              info,
              timestamp: timeSensitive ? timestamp : undefined,
            });
          }
        },
        markdownToHtml: markdownToHtml.toHTML,
      });

      setStrongProp(
        global,
        'PROTOCOLS_DIR',
        path.join(global.dataDir, '/protocols'),
      );

      // Global adaptable parseURL
      async function parseURL(url) {
        try {
          if (!url || !protocols) {
            return null;
          }
          let parsed = global.basicParseURL(url);
          if (!parsed.pathname || parsed.pathname === '') {
            parsed.pathname = '/';
          }
          if (!parsed.protocol || parsed.protocol.indexOf('undefined') === 0) {
            parsed.protocol = 'hyper://';
          }

          // TODO M - Come up with way to keep functions secure while being able to update protocols

          // TODO M - Resolve conflicting matching protocol prefixes by having user distinguish them
          for (var i = 0; i < protocols.length; i++) {
            const protocol = protocols[i];
            const protocolName = protocol.name;
            const protocolPrefix = protocol.prefix;
            const prefixRegex = new RegExp(protocolPrefix);
            if (prefixRegex.test(parsed.protocol) && protocol.parseURL) {
              const protocolParser = functionFromString({
                body: protocol.parseURL,
                name: `parseURL${protocolName}`,
              });
              console.log(
                '[NODE] typeof protocolParser:',
                typeof protocolParser,
              );
              try {
                const protocolParsed = await protocolParser(url);
                parsed = Object.assign(parsed, protocolParsed);
              } catch (error) {
                console.log(
                  '[NODE] URL Parse Error for protocol:',
                  protocolName,
                  error,
                );
              }
              break;
            }
          }

          return parsed;
        } catch (parseError) {
          throw parseError;
        }
      }
      setStrongProp(global, 'parseURL', parseURL);

      // Modify fetch functionality
      setStrongProp(global, 'basicFetch', fetch);
      setStrongProp(
        global,
        'fetch',
        new Proxy(fetch, fetchHandler(protocols, apis)),
      );
      // The query and write functions provide simpler, possibly more familiar, syntax
      // for common fetching cases (GET and PUT, respectively).
      const optionsToHeader = options => {
        let newOptions = {};
        for (const key in options) {
          let newKey = key.replace(/([a-z])([A-Z])/g, '$1-$2');
          const [firstLetter, ...rest] = newKey;
          newKey = [firstLetter.toLocaleUpperCase(), ...rest].join('');
          newOptions[newKey] = options[key];
        }
        return newOptions;
      };
      const queryOrWrite = async (method, ...params) => {
        let parameters = [...params];
        if (parameters.length === 0) {
          return;
        }
        let fetchResponse;
        if (parameters.length === 1) {
          const param = parameters[0];
          if (typeof param === 'string') {
            // Single Request
            fetchResponse = await fetch(param, {method});
          }
          if (typeof param === 'object') {
            // Single Request
            const newOptions = optionsToHeader(param.options);
            fetchResponse = await fetch(param.url, {...newOptions, method});
          } else {
            // Multiple Requests
            let newParam = param.map(p => {
              if (typeof p === 'object') {
                return optionsToHeader(p.options);
              } else {
                return optionsToHeader(p[1]);
              }
            });
            fetchResponse = await fetch(newParam);
          }
        } else {
          // Single Request
          const newOptions = optionsToHeader(parameters[1]);
          fetchResponse = await fetch(parameters[0], {...newOptions, method});
        }
        if (Array.isArray(fetchResponse)) {
          const fetchBodies = fetchResponse.map(res => res.body);
          return fetchBodies;
        }
        return fetchResponse.body;
      };
      const query = (...params) => queryOrWrite('GET', ...params);
      const write = (...params) => queryOrWrite('PUT', ...params);
      setStrongProp(global, 'query', query);
      setStrongProp(global, 'write', write);

      rn_bridge.channel.on(listeners.fetch, msg => {
        // NOTE - params should always at least have a unique ID for a callback to RN
        let {protocols: fetchProtocols, requestId, argumentsList} = msg;
        console.log(
          '[NODE] Starting fetch from bridge...',
          requestId,
          argumentsList,
        );
        fetchCallback({requestId, argumentsList, protocols: fetchProtocols});
      });

      rn_bridge.channel.on(listeners.parseURL, async msg => {
        // NOTE - params should always at least have a unique ID for a callback to RN
        let {protocols: parseProtocols, requestId, url} = msg;
        const parsed = await parseURL(url, parseProtocols);
        rn_bridge.channel.post(requestId, parsed);
      });

      rn_bridge.channel.on(
        listeners['api-request'],
        async ({requestId, apiName, args, currentUrl, fsPath}) => {
          let methodString = apis;
          for (var i = 0; i < apiName.length; i++) {
            const key = apiName[i];
            methodString = methodString[key];
          }
          const apiMethod = functionFromString({
            body: methodString,
            name: apiName.join(''),
            params: [currentUrl, fsPath, makeFsExport(apis)],
            paramNames: [
              'CURRENT_URL',
              'CURRENT_LOCAL_PATH',
              'EXPORT_TO_FILESYSTEM',
            ],
          });
          if (apiMethod && typeof apiMethod === 'function') {
            let apiResult = await apiMethod(...args);
            console.log(
              '[NODE] apiMethod Result:',
              apiResult,
              'for requestId:',
              requestId,
            );
            const bridgeResult = bridgify(apiResult, requestId);
            rn_bridge.channel.post(requestId, bridgeResult);
          }
        },
      );

      rn_bridge.channel.on(
        listeners['node-request'],
        async ({requestId, bridgeId, action}) => {
          let bridgeSource;
          if (bridgeId.indexOf('*') === -1) {
            bridgeSource = bridgeResults[bridgeId];
          } else {
            bridgeSource = bridgeResults;
            const bridgeIdKeys = bridgeId.split('*');
            for (var i = 0; i < bridgeIdKeys.length; i++) {
              let key = bridgeIdKeys[i];
              bridgeSource = bridgeSource[key];
            }
          }
          if (bridgeSource === undefined || !action || !action.type) {
            // TODO H - Handle invalid
          }
          if (action.eventName) {
            proxyEmitter(requestId, bridgeId, bridgeSource, action);
          } else {
            const result = await proxyAction(bridgeSource, action);
            console.log(
              '[NODE] proxyAction Result:',
              result,
              'for requestId:',
              requestId,
            );
            if (result !== '__DO_NOT_RETURN__') {
              const bridgeResult = bridgify(result, requestId);
              rn_bridge.channel.post(requestId, bridgeResult);
            }
          }
        },
      );

      // Setup parent folders
      fse.emptyDirSync(global.tmpDir);

      // HACK - Install missing babel plugins [BABEL IS NO LONGER NEEDED]
      // const docModulesPath = path.join(global.dataDir, '/node_modules');
      // const docModulesExist = fs.existsSync(docModulesPath);
      // if (!docModulesExist) {
      //   console.log('[NODE] Writing Modules to Documents folder...');
      //   await writeDocumentsModules();
      //   console.log('[NODE] Done writing Documents Modules!');
      // }

      for (var i = 0; i < protocols.length; i++) {
        const protocol = protocols[i];
        const protocolName = protocol.name;
        if (typeof protocol === 'string') {
          // TODO M - Get JSON from reference
        }
        if (protocol.setup) {
          // TODO M? - Wrap 'setProtocol' in async function that contains all this in a try/catch?
          const permDir = path.join(global.PROTOCOLS_DIR, '/' + protocolName);
          const permDirExists = fs.existsSync(permDir);
          if (!permDirExists) {
            require('fs').mkdirSync(permDir, {recursive: true});
          }
          const tempDir = path.join(
            global.tmpDir,
            '/protocols/' + protocolName,
          );
          const tempDirExists = fs.existsSync(tempDir);
          if (!tempDirExists) {
            require('fs').mkdirSync(tempDir, {recursive: true});
          }
          const setProtocol = await functionFromString({
            body: protocol.setup,
            name: `setup${protocolName}`,
            params: [permDir, tempDir, makeFsExport(apis)],
            paramNames: ['PERM_DIR', 'TEMP_DIR', 'EXPORT_TO_FILESYSTEM'],
          })(settings[protocolName]);
          setStrongProp(global, protocolName, setProtocol);
        }
      }

      // TODO H - Combine all dependencies (prioritizing hyper > local) into categories: local and hyper
      //        - Install any missing dependencies using respective methods

      // TEST - HYPERSWARM FUNCTIONALITY:
      // const airpipe = require('airpipe');
      // airpipe.connect('calm-rad-test', (...params) => {
      //   console.log('[AIRPIPE] Callback:', [...params]);
      // });

      console.log('[NODE] Done with setup function!');
    } catch (setupError) {
      console.log('[NODE] SETUP ERROR: setupError', setupError);
      rn_bridge.channel.post(listeners['node-error'], {
        error: formatError(setupError),
      });
    }
  }
}

rn_bridge.channel.on('setup-node', async options => {
  await setup(options);
  rn_bridge.channel.post(
    options.listeners['setup-callback'],
    'NODE IS SET UP!',
  );
});
