const fse = require('fs-extra');
const acorn = require('acorn');
const acornWalk = require('acorn-walk');
const acornLoose = require('acorn-loose');
var acornGlobals = require('acorn-globals');
var estreeAssignParent = require('estree-assign-parent');
var getAssignedIdentifiers = require('get-assigned-identifiers');
var isFunction = require('estree-is-function');
var scopeAnalyzer = require('./scope-analyzer/index.js');

// TODO - Only awaitify descendents of APIs and fetch, asyncify any parent functions of these cases that aren't already async (in those cases, awaitify everything in those functions)
//          This will limit the room for parsing error, minimize parse time, ensure any proxied variables are taken care of without being overkill, and just generally be more reliable
//          This will also avoid the problem of needing to follow trails of functions that shouldn't be asyncified
//        - STEPS: When asyncifying, note which functions have been made asynchronous. All functions that call these functions must be made async and await just those functions (recursive)
//                 acornWalk.findNodeAround and just work off of those nodes. Saving vars to change as we go
//                     ALT: Identify where these results point to: Variable assignment, 'then', or callbacks in arguments
//                 Awaitify all the chosen nodes
// function mergeApiArrays(...arrays) {
//   let jointArray = [];
//   arrays.forEach(array => {
//     jointArray = [...jointArray, ...array];
//   });
//   const uniqueArray = jointArray.filter(
//     (item, index) =>
//       jointArray.findIndex(el => {
//         return el.index === item.index && el[0] === item[0];
//       }) === index,
//   );
//   return uniqueArray;
// }
// const apiToRegex = apis => {
//   var regexes = [];
//   var windowPrefix = '(?:\\bwindow\\b[\\n \\s]*\\??[\\n \\s]*\\.[\\n \\s]*)?';
//   const regexifyObject = (obj, parentName) => {
//     if (typeof obj === 'string') {
//       regexes.push(new RegExp(windowPrefix + '\\b' + parentName + '\\b', 'g'));
//     } else {
//       for (const key in obj) {
//         const val = obj[key];
//         const keyWithParent =
//           parentName + '[\\n \\s]*\\??[\\n \\s]*\\.[\\n \\s]*\\b' + key + '\\b';
//         regexes.push(new RegExp(windowPrefix + keyWithParent, 'g'));
//         if (typeof val === 'object') {
//           regexifyObject(val, keyWithParent);
//         }
//       }
//     }
//   };
//   Object.entries(apis).forEach(([key, value]) => {
//     regexifyObject(value, key);
//   });
//   return regexes;
// };
// const modifyNewJsChunk = (newChunk, apis) => {
//   console.log('[NODE] modifyNewJsChunk:', newChunk);
//   newChunk = newChunk
//     .replace(
//       /console\.log\((["'`]|JSON.stringify\()/g,
//       'window.ReactNativeWebView.postMessage("[WEBVIEW-LOG] " + $1',
//     )
//     .replace(
//       /console\.debug\((["'`]|JSON.stringify\()/g,
//       'window.ReactNativeWebView.postMessage("[WEBVIEW-DEBUG] " + $1',
//     )
//     .replace(
//       /console\.error\((["'`]|JSON.stringify\()/g,
//       'window.ReactNativeWebView.postMessage("[WEBVIEW-ERROR] " + $1',
//     );
//   const ILLEGAL_FUNCTIONS = ['constructor', 'get'];
//   const ILLEGAL_NAMES = ['then', 'catch', 'next', 'undefined', 'null', '✖'];
//   let startNode = acornParse(newChunk);
//   const apiRegexes = apiToRegex(apis);
//   const getBridgeApiMatches = (walkNode, chunk, changedNames = []) => {
//     var matches = [
//       ...newChunk.matchAll(
//         /(?:\bwindow\b[\n \s]*\??[\n \s]*\.[\n \s]*)?\bfetch\b/g,
//       ),
//     ];
//     apiRegexes.forEach((regexp, i) => {
//       let regexMatches = [...chunk.matchAll(regexp)];
//       matches = mergeApiArrays(matches, regexMatches);
//     });
//     changedNames.forEach((name, i) => {
//       const regexp = new RegExp('\\b' + name + '\\b', 'g');
//       let regexMatches = [...chunk.matchAll(regexp)];
//       matches = mergeApiArrays(matches, regexMatches);
//     });
//     return matches;
//   };
//   let apiMatches = getBridgeApiMatches(startNode, newChunk);
//   if (apiMatches.length > 0) {
//     let changedVarNames = [];
//     // Asyncify
//     const walkNodeAndAsyncifyString = (walkNode, chunk, chunkOffset = 0) => {
//       console.log(
//         '[NODE] Initial Asyncify Walk Node:',
//         walkNode,
//         'For chunk:',
//         chunk,
//         'With offset:',
//         chunkOffset,
//       );
//       if (walkNode && !(walkNode instanceof SyntaxError)) {
//         var nodesToChange = [];
//         var skipFunctions = [];
//         var walkedNodes = [];
//         var illegalFuncRanges = [];
//         var newExpressions = [];
//         var declaredFuncVariables = [];
//         acornWalk.full(walkNode, node => {
//           walkedNodes.push(node);
//         });
//         var sortedWalkNodes = walkedNodes.sort((a, b) => {
//           if (a.start === b.start) {
//             return b.end - a.end;
//           }
//           return a.start - b.start;
//         });
//         var newMatches = [apiMatches];
//         var matchIteration = -1;
//         while (newMatches.length) {
//           matchIteration += 1;
//           let scopeMatches = newMatches.pop();
//           console.log(
//             '[NODE] Checking scopeMatches for iteration:',
//             matchIteration,
//             scopeMatches,
//           );
//           sortedWalkNodes.forEach(node => {
//             if (
//               node.type === 'NewExpression' &&
//               node.callee.type === 'Identifier' &&
//               matchIteration === 0
//             ) {
//               console.log('[NODE] Added NewExpression:', node.callee);
//               newExpressions.push(node.callee);
//             } else if (
//               node.type === 'BinaryExpression' &&
//               node.operator === 'instanceof' &&
//               node.right.type === 'Identifier' &&
//               matchIteration === 0
//             ) {
//               console.log('[NODE] Added instanceof:', node.right);
//               newExpressions.push(node.right);
//             } else if (
//               node.type === 'VariableDeclarator' &&
//               node.init &&
//               ((node.init.type === 'FunctionExpression' && !node.init.async) ||
//                 (node.init.type === 'ArrowFunctionExpression' &&
//                   !node.init.async)) &&
//               matchIteration === 0
//             ) {
//               declaredFuncVariables.push(node);
//             } else if (
//               node.type === 'Property' &&
//               node.kind === 'get' &&
//               matchIteration === 0
//             ) {
//               illegalFuncRanges.push({start: node.start, end: node.end});
//             } else if (
//               (node.type === 'FunctionExpression' && !node.async) ||
//               (node.type === 'ArrowFunctionExpression' && !node.async) ||
//               (node.type === 'FunctionDeclaration' && !node.async) ||
//               (node.type === 'MethodDefinition' &&
//                 node.value &&
//                 (node.value.type === 'FunctionExpression' ||
//                   node.value.type === 'ArrowFunctionExpression' ||
//                   node.value.type === 'FunctionDeclaration') &&
//                 !node.value.async)
//             ) {
//               if (node.value) {
//                 const valId =
//                   node.value.type + node.value.start + node.value.end;
//                 skipFunctions.push(valId);
//               }
//               const funcName = node.key
//                 ? node.key.name
//                 : node.id
//                 ? node.id.name
//                 : node.name;
//               const body = node.value ? node.value.body : node.body;
//               const hasApiCall =
//                 scopeMatches.filter(apiMatch => {
//                   return (
//                     body &&
//                     apiMatch.index >= body.start &&
//                     apiMatch.index <= body.end
//                   );
//                 }).length > 0;
//               if (
//                 hasApiCall &&
//                 (!funcName || ILLEGAL_FUNCTIONS.indexOf(funcName) === -1)
//               ) {
//                 if (funcName && changedVarNames.indexOf(funcName) === -1) {
//                   changedVarNames.push(funcName);
//                   newMatches.push([
//                     ...chunk.matchAll(
//                       new RegExp('\\b' + funcName + '\\b', 'g'),
//                     ),
//                   ]);
//                   console.log(
//                     '[NODE] Adding name:',
//                     funcName,
//                     ' to while-loop!',
//                   );
//                 } else if (changedVarNames.indexOf(funcName) === -1) {
//                   let declarationTest = declaredFuncVariables.filter(obj => {
//                     return (
//                       obj.init.start === node.start &&
//                       obj.init.end === node.end &&
//                       obj.init.type === node.type
//                     );
//                   });
//                   if (declarationTest.length > 0) {
//                     declarationTest.forEach((variable, i) => {
//                       if (
//                         variable.id &&
//                         variable.id.name &&
//                         changedVarNames.indexOf(variable.id.name) === -1
//                       ) {
//                         changedVarNames.push(variable.id.name);
//                         newMatches.push([
//                           ...chunk.matchAll(
//                             new RegExp('\\b' + variable.id.name + '\\b', 'g'),
//                           ),
//                         ]);
//                         console.log(
//                           '[NODE] Adding name:',
//                           variable.id.name,
//                           ' to while-loop!',
//                         );
//                       }
//                     });
//                   }
//                 }
//                 nodesToChange.unshift(node);
//               } else {
//                 illegalFuncRanges.push({start: node.start, end: node.end});
//               }
//             }
//           });
//         }
//         console.log('[NODE] Done with Asyncify while-loop!');
//         // Modify Functions
//         nodesToChange
//           .sort((a, b) => {
//             return b.start - a.start;
//           })
//           .forEach(node => {
//             const isIllegal =
//               illegalFuncRanges.filter(obj => {
//                 return node.start >= obj.start && node.end <= obj.end;
//               }).length > 0;
//             const currentNodeId = node.type + node.start + node.end;
//             const skipFunc = skipFunctions.indexOf(currentNodeId) !== -1;
//             let declarationTest = declaredFuncVariables.filter(obj => {
//               return (
//                 obj.init.type === node.type &&
//                 obj.init.start === node.start &&
//                 obj.init.end === node.end
//               );
//             });
//             let possibleConstructor = declarationTest.some(dec => {
//               return (
//                 newExpressions.filter(obj => {
//                   return obj.start > dec.id.start && obj.name === dec.id.name;
//                 }).length > 0
//               );
//             });
//             console.log(
//               '[NODE] Checking conditions for function:',
//               node,
//               'Not Illegal:',
//               !isIllegal,
//               'Not Skipped:',
//               !skipFunc,
//               'Not a Constructor:',
//               !possibleConstructor,
//             );
//             if (!isIllegal && !skipFunc && !possibleConstructor) {
//               let baseStart = node.start - chunkOffset;
//               let baseEnd = node.end - chunkOffset;
//               console.log(
//                 '[NODE] Original End:',
//                 node.end,
//                 'New end:',
//                 baseEnd,
//               );
//               let prevNode = chunk.substring(baseStart, baseEnd);
//               let newNode = 'async ' + prevNode;
//               console.log(
//                 '[NODE] Pre-Modified function:',
//                 prevNode,
//                 'for node:',
//                 node,
//                 'with offset:',
//                 chunkOffset,
//               );
//               chunk =
//                 chunk.substring(0, baseStart) +
//                 newNode +
//                 chunk.substring(baseEnd);
//             }
//           });
//         chunk = chunk.replace(/(async[\s]+){2,}/g, 'async ');
//       }
//       return chunk;
//     };
//     newChunk = walkNodeAndAsyncifyString(startNode, newChunk);
//     // Awaitify
//     startNode = acornParse(newChunk);
//     apiMatches = getBridgeApiMatches(startNode, newChunk, changedVarNames);
//     var illegalRanges = [];
//     var illegalChildren = [];
//     var protectedChildren = [];
//     var functionBodyRanges = [];
//     var modifiedNodes = [];
//     var addedLengths = {};
//     const walkNodeAndAwaitifyString = (walkNode, chunk, chunkOffset = 0) => {
//       console.log(
//         '[NODE] Initial Awaitify Walk Node:',
//         walkNode,
//         'For chunk:',
//         chunk,
//         'With offset:',
//         chunkOffset,
//       );
//       if (walkNode && !(walkNode instanceof SyntaxError)) {
//         var nodesToChange = [];
//         var skipNodes = [];
//         var walkedNodes = [];
//         acornWalk.full(walkNode, node => {
//           walkedNodes.push(node);
//         });
//         let sortedWalkNodes = walkedNodes.sort((a, b) => {
//           if (a.start === b.start) {
//             return b.end - a.end;
//           }
//           return a.start - b.start;
//         });
//         sortedWalkNodes.forEach(node => {
//           if (
//             (node.type === 'FunctionExpression' && node.async) ||
//             (node.type === 'ArrowFunctionExpression' && node.async) ||
//             (node.type === 'FunctionDeclaration' && node.async) ||
//             (node.type === 'MethodDefinition' &&
//               node.value &&
//               (node.value.type === 'FunctionExpression' ||
//                 node.value.type === 'ArrowFunctionExpression' ||
//                 node.value.type === 'FunctionDeclaration') &&
//               node.value.async)
//           ) {
//             if (node.key) {
//               const keyId = node.key.type + node.key.start + node.key.end;
//               skipNodes.push(keyId);
//             }
//             const body = node.value ? node.value.body : node.body;
//             const hasApiCall =
//               apiMatches.filter(apiMatch => {
//                 return (
//                   body &&
//                   apiMatch.index >= body.start &&
//                   apiMatch.index <= body.end
//                 );
//               }).length > 0;
//             if (hasApiCall) {
//               console.log('[NODE] Added body for node:', node);
//               functionBodyRanges.push({start: body.start, end: body.end});
//             }
//           }
//         });
//         let selectiveWalkNodes = [];
//         apiMatches.forEach((apiMatch, i) => {
//           acornWalk.findNodeAround(
//             walkNode,
//             apiMatch.index,
//             (nodeType, node) => {
//               if (
//                 nodeType !== 'FunctionExpression' &&
//                 nodeType !== 'ArrowFunctionExpression' &&
//                 nodeType !== 'FunctionDeclaration' &&
//                 nodeType !== 'MethodDefinition'
//               ) {
//                 selectiveWalkNodes.push(node);
//               } else if (node.key) {
//                 const keyId = node.key.type + node.key.start + node.key.end;
//                 skipNodes.push(keyId);
//               }
//               return false;
//             },
//           );
//         });
//         selectiveWalkNodes = selectiveWalkNodes.sort((a, b) => {
//           if (a.start === b.start) {
//             return b.end - a.end;
//           }
//           return a.start - b.start;
//         });
//         selectiveWalkNodes.forEach(node => {
//           console.log('[NODE] Acorn Node:', node);
//           if (node.type === 'Property' && node.kind === 'get') {
//             illegalRanges.push({start: node.start, end: node.end});
//           } else if (
//             (node.type === 'FunctionExpression' && !node.async) ||
//             (node.type === 'ArrowFunctionExpression' && !node.async) ||
//             (node.type === 'FunctionDeclaration' && !node.async) ||
//             (node.type === 'MethodDefinition' &&
//               node.value &&
//               (node.value.type === 'FunctionExpression' ||
//                 node.value.type === 'ArrowFunctionExpression' ||
//                 node.value.type === 'FunctionDeclaration') &&
//               !node.value.async)
//           ) {
//             if (node.key) {
//               const keyId = node.key.type + node.key.start + node.key.end;
//               skipNodes.push(keyId);
//             }
//             illegalRanges.push({start: node.start, end: node.end});
//           } else {
//             if (node.type === 'AssignmentExpression') {
//               illegalRanges.push({start: node.start, end: node.left.end});
//             }
//             if (node.type === 'Property') {
//               const keyId = node.key.type + node.key.start + node.key.end;
//               const valId = node.value.type + node.value.start + node.value.end;
//               if (keyId === valId) {
//                 nodesToChange.unshift(node);
//                 skipNodes.push(keyId);
//               }
//             }
//             // if (
//             //   node.type === 'NewExpression' &&
//             //   node.callee.type === 'Identifier'
//             // ) {
//             //   const calleeId =
//             //     node.callee.type + node.callee.start + node.callee.end;
//             //   skipNodes.push(calleeId);
//             // }
//             if (
//               node.type === 'ConditionalExpression' &&
//               node.alternate.type === 'Identifier' &&
//               node.alternate.name === '✖' &&
//               node.test.type !== 'Identifier' &&
//               node.test.type !== 'CallExpression' &&
//               node.test.type !== 'MemberExpression'
//             ) {
//               nodesToChange.unshift(node.test);
//             }
//             const nodeName = node.callee
//               ? node.callee.property
//                 ? node.callee.property.name
//                 : node.callee.name
//               : node.property
//               ? node.property.name
//               : node.name
//               ? node.name
//               : node.type;
//             if (ILLEGAL_NAMES.indexOf(nodeName) !== -1 && node.callee) {
//               console.log('[NODE] Illegal node found:', node);
//               const filteredNode =
//                 node.callee.type + node.callee.start + node.callee.end;
//               illegalChildren.push(filteredNode);
//               if (node.callee.object) {
//                 const filteredNodeObject =
//                   node.callee.object.type +
//                   node.callee.object.start +
//                   node.callee.object.end;
//                 illegalChildren.push(filteredNodeObject);
//               }
//               if (node.callee.callee) {
//                 const filteredNodeCallee =
//                   node.callee.callee.type +
//                   node.callee.callee.start +
//                   node.callee.callee.end;
//                 illegalChildren.push(filteredNodeCallee);
//               }
//               if (node.arguments) {
//                 node.arguments.forEach(childNode => {
//                   acornWalk.full(childNode, childWalkNode => {
//                     const protectedNodeId =
//                       childWalkNode.type +
//                       childWalkNode.start +
//                       childWalkNode.end;
//                     console.log('[NODE] Protecting child:', protectedNodeId);
//                     protectedChildren.push(protectedNodeId);
//                   });
//                 });
//               }
//             }
//             const nodeId = node.type + node.start + node.end;
//             // TODO? - Maybe add 'ThisExpression' to accepted object types
//             if (
//               ((node.type === 'MemberExpression' &&
//                 (node.object.type === 'Identifier' ||
//                   node.object.type === 'MemberExpression')) ||
//                 (node.type === 'CallExpression' &&
//                   (node.callee.type === 'Identifier' ||
//                     (node.callee.type === 'MemberExpression' &&
//                       (node.callee.object.type === 'Identifier' ||
//                         node.callee.object.type === 'MemberExpression' ||
//                         node.callee.object.type === 'CallExpression'))))) &&
//               functionBodyRanges.filter(obj => {
//                 return node.start >= obj.start && node.end <= obj.end;
//               }).length > 0
//             ) {
//               const skipAllAncestors = parentNode => {
//                 acornWalk.full(parentNode, child => {
//                   const childId = child.type + child.start + child.end;
//                   const protectionIndex = protectedChildren.indexOf(childId);
//                   if (
//                     // This is comparing it to the initial nodeId, not immediate parent, which is fine
//                     childId !== nodeId &&
//                     protectionIndex === -1
//                   ) {
//                     console.log('[NODE] Skipped Child:', childId);
//                     skipNodes.push(childId);
//                     // if (child.property) {
//                     //   skipAllAncestors(child.property);
//                     // }
//                     // if (child.object) {
//                     //   skipAllAncestors(child.object);
//                     // }
//                     // if (child.callee) {
//                     //   if (child.callee.property) {
//                     //     skipAllAncestors(child.callee.property);
//                     //   }
//                     //   if (child.callee.object) {
//                     //     skipAllAncestors(child.callee.object);
//                     //   }
//                     // }
//                   }
//                 });
//                 protectedChildren = [];
//               };
//               skipAllAncestors(node);
//               nodesToChange.unshift(node);
//             }
//             if (node.type === 'Identifier') {
//               nodesToChange.unshift(node);
//             }
//           }
//         });
//         console.log('[NODE] illegalChildren:', illegalChildren);
//         // Modify Nodes
//         nodesToChange = nodesToChange
//           .sort((a, b) => {
//             return b.start - a.start;
//           })
//           .filter(node => {
//             const currentNodeId = node.type + node.start + node.end;
//             const skip = skipNodes.indexOf(currentNodeId) !== -1;
//             const isIllegal =
//               illegalRanges.filter(obj => {
//                 return node.start >= obj.start && node.end <= obj.end;
//               }).length > 0;
//             const isValidBody =
//               functionBodyRanges.filter(obj => {
//                 return node.start >= obj.start && node.end <= obj.end;
//               }).length > 0;
//             return isValidBody && !isIllegal && !skip;
//           });
//         for (var nodeIndex = 0; nodeIndex < nodesToChange.length; nodeIndex++) {
//           var node = nodesToChange[nodeIndex];
//           const nodeName = node.callee
//             ? node.callee.property
//               ? node.callee.property.name
//               : node.callee.name
//             : node.property
//             ? node.property.name
//             : node.name
//             ? node.name
//             : node.type;
//           const currentNodeId = node.type + node.start + node.end;
//           const modified = modifiedNodes.indexOf(currentNodeId) !== -1;
//           console.log(
//             '[NODE] Checking conditions for node:',
//             node,
//             'Not modified:',
//             !modified,
//             'After offset:',
//             node.start >= chunkOffset,
//             'Not illegal child:',
//             illegalChildren.indexOf(currentNodeId) === -1,
//           );
//           if (
//             !modified &&
//             node.start >= chunkOffset &&
//             ILLEGAL_NAMES.indexOf(nodeName) === -1 &&
//             illegalChildren.indexOf(currentNodeId) === -1 &&
//             (node.type === 'Identifier' ||
//               node.type === 'MemberExpression' ||
//               node.type === 'CallExpression')
//           ) {
//             let baseStart = node.start - chunkOffset;
//             let baseEnd = node.end - chunkOffset;
//             console.log('[NODE] Original End:', node.end, 'New end:', baseEnd);
//             let prevNode = chunk.substring(baseStart, baseEnd);
//             let newNode = prevNode.slice();
//             console.log(
//               '[NODE] Pre-Modified code:',
//               prevNode,
//               'for node:',
//               node,
//               'with offset:',
//               chunkOffset,
//             );
//             const getNodeChain = (parentNode, chainArray = []) => {
//               var chainedIds = [];
//               var parent = [parentNode];
//               var nextIndex = nodeIndex;
//               var scopeNode;
//               while (parent.length) {
//                 scopeNode = parent.pop();
//                 const scopeNodeId =
//                   scopeNode.type + scopeNode.start + scopeNode.end;
//                 chainedIds.push(scopeNodeId);
//                 const scopeNodeName = scopeNode.callee
//                   ? scopeNode.callee.property
//                     ? scopeNode.callee.property.name
//                     : scopeNode.callee.name
//                   : scopeNode.property
//                   ? scopeNode.property.name
//                   : scopeNode.name
//                   ? scopeNode.name
//                   : scopeNode.type;
//                 console.log(
//                   '[NODE] getNodeChain for node:',
//                   scopeNode,
//                   'named:',
//                   scopeNodeName,
//                 );
//                 if (scopeNode.arguments) {
//                   let newEnd = scopeNode.end;
//                   scopeNode.arguments.forEach(argNode => {
//                     acornWalk.full(argNode, child => {
//                       const childId = child.type + child.start + child.end;
//                       if (addedLengths[childId]) {
//                         console.log(
//                           '[NODE] Adding length:',
//                           addedLengths[childId],
//                           'for child:',
//                           childId,
//                         );
//                         newEnd += addedLengths[childId];
//                       }
//                     });
//                   });
//                   scopeNode = Object.assign(scopeNode, {end: newEnd});
//                 }
//                 if (scopeNode.type === 'Identifier' && scopeNode.name === '✖') {
//                   chainArray.push({
//                     start: scopeNode.start,
//                     end: scopeNode.end,
//                     node: scopeNode,
//                     awaitify: false,
//                   });
//                   nextIndex += 1;
//                   const nextNode = nodesToChange[nextIndex];
//                   console.log('[NODE] Next potential chain node:', nextNode);
//                   if (nextNode) {
//                     // Assume it's part of a Conditional chain
//                     parent.push(nextNode);
//                   }
//                 } else if (
//                   scopeNode.callee &&
//                   scopeNode.callee.type === 'MemberExpression'
//                 ) {
//                   chainArray.push({
//                     start: scopeNode.callee.property.start,
//                     end: scopeNode.end,
//                     arguments: scopeNode.arguments,
//                     node: scopeNode,
//                     awaitify:
//                       ILLEGAL_NAMES.indexOf(scopeNodeName) === -1 &&
//                       illegalChildren.indexOf(scopeNodeId) === -1,
//                   });
//                   parent.push(scopeNode.callee.object);
//                 } else if (scopeNode.type === 'MemberExpression') {
//                   chainArray.push({
//                     start: scopeNode.property.start,
//                     end: scopeNode.end,
//                     node: scopeNode,
//                     awaitify:
//                       (scopeNode.property.type === 'Identifier' ||
//                         scopeNode.property.type === 'CallExpression') &&
//                       (ILLEGAL_NAMES.indexOf(scopeNodeName) === -1 &&
//                         illegalChildren.indexOf(scopeNodeId) === -1),
//                   });
//                   parent.push(scopeNode.object);
//                 } else {
//                   // Done with chain
//                   const newStart =
//                     scopeNode.type === 'CallExpression' && scopeNode.callee
//                       ? scopeNode.callee.start
//                       : scopeNode.start;
//                   chainArray.push({
//                     start: newStart,
//                     end: scopeNode.end,
//                     arguments:
//                       scopeNode.type !== 'Identifier' &&
//                       scopeNode.type !== 'CallExpression' &&
//                       scopeNode.type !== 'ThisExpression'
//                         ? [scopeNode]
//                         : scopeNode.arguments,
//                     node: scopeNode,
//                     awaitify:
//                       (scopeNode.type === 'Identifier' ||
//                         scopeNode.type === 'CallExpression') &&
//                       (ILLEGAL_NAMES.indexOf(scopeNodeName) === -1 &&
//                         illegalChildren.indexOf(scopeNodeId) === -1),
//                   });
//                 }
//               }
//               return chainArray;
//             };
//             // Modify longest chain of MemberExpression
//             const modifyMemberExpression = (scopeNode, scopeString) => {
//               const chainArray = getNodeChain(scopeNode);
//               console.log(
//                 '[NODE] Chain Array:',
//                 chainArray,
//                 'for node:',
//                 scopeNode,
//               );
//               let chainStart =
//                 chainArray[chainArray.length - 1].start - chunkOffset;
//               let chainEnd = chainArray[0].end - chunkOffset;
//               console.log(
//                 '[NODE] Updating baseStart from:',
//                 baseStart,
//                 'to:',
//                 chainStart,
//                 'where chunkOffset:',
//                 chunkOffset,
//               );
//               console.log(
//                 '[NODE] Updating baseEnd from:',
//                 baseEnd,
//                 'to:',
//                 chainEnd,
//                 'where chunkOffset:',
//                 chunkOffset,
//               );
//               baseStart = chainStart;
//               baseEnd = chainEnd;
//               scopeString = chunk.substring(chainStart, chainEnd);
//               let newScopeString = scopeString.slice();
//               console.log(
//                 '[NODE] Unedited Chain String:',
//                 scopeString,
//                 'From chunk:',
//                 chunk,
//                 'Start:',
//                 chainStart,
//                 'End:',
//                 chainEnd,
//               );
//               let startDisplacement = 0;
//               // TODO - BUG: Last member of ConditionalExpression chain doesn't get awaitified
//               //          REASON: await is properly added in replacement string, but is being cut off somehow
//               //          NOTE: Not an issue with baseStart/baseEnd, meaning there may be an unforseen change from the new 'arguments' being modified
//               chainArray.forEach((chainNode, chainIndex) => {
//                 const chainNodeId =
//                   chainNode.node.type +
//                   chainNode.node.start +
//                   chainNode.node.end;
//                 const chainNodeModified =
//                   modifiedNodes.indexOf(chainNodeId) !== -1;
//                 if (!chainNodeModified) {
//                   if (chainNode.awaitify && chainIndex !== 0) {
//                     const displacedEnd =
//                       chainNode.end - chunkOffset + startDisplacement;
//                     const relativeEnd = displacedEnd - chainStart;
//                     newScopeString =
//                       newScopeString.slice(0, relativeEnd) +
//                       ')' +
//                       newScopeString.slice(relativeEnd);
//                     console.log(
//                       '[NODE] End Parenth Added:',
//                       newScopeString,
//                       'end:',
//                       chainNode.end,
//                       'displacedEnd:',
//                       displacedEnd,
//                       'relativeEnd:',
//                       relativeEnd,
//                     );
//                   }
//                   if (chainNode.arguments) {
//                     console.log('[NODE] Chain Pre-Arguments:', newScopeString);
//                     const callStart =
//                       chainNode.start - chunkOffset + startDisplacement;
//                     const callEnd =
//                       chainNode.end - chunkOffset + startDisplacement;
//                     const relativeStart = callStart - chainStart;
//                     const relativeEnd = callEnd - chainStart;
//                     let callString = newScopeString.slice(
//                       relativeStart,
//                       relativeEnd,
//                     );
//                     callString = modifyCallArguments(
//                       chainNode.arguments,
//                       callString,
//                       chainNode.start, // chunkOffset + (callStart - startDisplacement),
//                     );
//                     newScopeString =
//                       newScopeString.slice(0, relativeStart) +
//                       callString +
//                       newScopeString.slice(relativeEnd);
//                     console.log('[NODE] Chain Post-Arguments:', newScopeString);
//                   }
//                   if (chainNode.awaitify && chainIndex === 0) {
//                     newScopeString = 'await ' + newScopeString;
//                     startDisplacement += 6;
//                     modifiedNodes.push(chainNodeId);
//                     console.log('[NODE] Start await added:', newScopeString);
//                   } else if (chainNode.awaitify) {
//                     newScopeString =
//                       newScopeString.slice(0, startDisplacement) +
//                       '(await ' +
//                       newScopeString.slice(startDisplacement);
//                     startDisplacement += 7;
//                     modifiedNodes.push(chainNodeId);
//                     console.log('[NODE] Parenth await added:', newScopeString);
//                   }
//                 }
//               });
//               console.log(
//                 '[NODE] Finished chain string:',
//                 newScopeString,
//                 'From:',
//                 scopeString,
//               );
//               const newLength = newScopeString.length - scopeString.length;
//               const scopeNodeId =
//                 scopeNode.type + scopeNode.start + scopeNode.end;
//               addedLengths[scopeNodeId] = newLength;
//               modifiedNodes.push(scopeNodeId);
//               return newScopeString;
//             };
//             // Modify arguments of CallExpression
//             const modifyCallArguments = (
//               args,
//               scopeString,
//               absStartOfScopeStr,
//             ) => {
//               console.log(
//                 '[NODE] modify call args:',
//                 args,
//                 'from scopeString:',
//                 scopeString,
//                 'offset by:',
//                 absStartOfScopeStr,
//               );
//               const reverseArgs = args.sort((a, b) => {
//                 return b.start - a.start;
//               });
//               reverseArgs.forEach((argNode, i) => {
//                 const relativeStart = argNode.start - absStartOfScopeStr;
//                 const relativeEnd = argNode.end - absStartOfScopeStr;
//                 let argString = scopeString.slice(relativeStart, relativeEnd);
//                 argString = walkNodeAndAwaitifyString(
//                   argNode,
//                   argString,
//                   argNode.start,
//                 );
//                 scopeString =
//                   scopeString.slice(0, relativeStart) +
//                   argString +
//                   scopeString.slice(relativeEnd);
//               });
//               return scopeString;
//             };
//             const modifySingleNode = (scopeNode, scopeString) => {
//               let newScopeString = 'await ' + scopeString;
//               if (scopeNode.type === 'Property') {
//                 newScopeString = scopeNode.key.name + ': await ' + scopeString;
//               }
//               const newLength = newScopeString.length - scopeString.length;
//               const scopeNodeId =
//                 scopeNode.type + scopeNode.start + scopeNode.end;
//               addedLengths[scopeNodeId] = newLength;
//               modifiedNodes.push(scopeNodeId);
//               return newScopeString;
//             };
//             if (
//               node.type.indexOf('MemberExpression') !== -1 ||
//               (node.callee &&
//                 node.callee.type.indexOf('MemberExpression') !== -1)
//             ) {
//               // Chain
//               newNode = modifyMemberExpression(node, newNode);
//             } else {
//               // Single
//               if (node.type === 'CallExpression') {
//                 newNode = modifyCallArguments(
//                   node.arguments,
//                   newNode,
//                   node.start, //chunkOffset + baseStart,
//                 );
//               }
//               newNode = modifySingleNode(node, newNode);
//             }
//             chunk =
//               chunk.substring(0, baseStart) +
//               newNode +
//               chunk.substring(baseEnd);
//           }
//         }
//         chunk = chunk
//           .replace(/(await[\s]+){2,}/g, 'await ')
//           .replace(/new[\n \s]+await[\s]/g, 'await new ');
//       }
//       return chunk;
//     };
//     return walkNodeAndAwaitifyString(startNode, newChunk);
//   } else {
//     console.log('[NODE] No API calls!');
//   }
//   return newChunk;
// };

// START

const acornParse = (string) => {
  let node;
  let acornOpts = {
    ecmaVersion: 'latest',
    preserveParens: false,
    allowHashBang: true,
    allowImportExportEverywhere: true,
    allowReturnOutsideFunction: true,
    allowAwaitOutsideFunction: true,
    // locations: true,
    sourceType: 'module',
  };
  try {
    node = acorn.parse(string, acornOpts);
  } catch (parseError) {
    console.log('[NODE] Acorn Parse Error:', parseError);
    try {
      node = acornLoose.parse(string, acornOpts);
    } catch (looseError) {
      console.log('[NODE] Acorn Loose Parse Error:', looseError);
      acornOpts.sourceType = 'script';
      try {
        node = acorn.parse(string, acornOpts);
      } catch (parseError2) {
        console.log('[NODE] Acorn Parse Error 2:', parseError2);
        try {
          node = acornLoose.parse(string, acornOpts);
        } catch (looseError2) {
          console.log('[NODE] Acorn Loose Parse Error 2:', looseError2);
        }
      }
    }
  }
  return node;
};

const getIdentifierChain = (_node, reverse) => {
  let awaitStart = _node.start;
  let argsToEval = [];
  let node = _node;
  while (node.parent) {
    if (scopeAnalyzer.isReferenceNode(node.parent)) {
      node = node.parent;
    } else {
      break;
    }
  }
  let chain = [];
  let remaining = [node];
  let extended;
  while (remaining.length) {
    let current = remaining.pop();
    if (current.type === 'AwaitExpression') {
      remaining.push(current.argument);
    } else if (
      current.type === 'ChainExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      remaining.push(current.expression);
    } else if (current.type === 'CallExpression') {
      remaining.push(current.callee);
      extended = {end: current.end, arguments: current.arguments};
    } else if (current.type === 'MemberExpression') {
      let property = current.property;
      if (extended) {
        property.end = extended.end;
        property.arguments = extended.arguments;
        extended = undefined;
        if (property.start >= awaitStart) {
          argsToEval.push(...property.arguments);
        }
      }
      chain.unshift(current.property);
      if (scopeAnalyzer.isReferenceNode(current.object)) {
        remaining.push(current.object);
      } else {
        chain.unshift(current.object);
      }
    } else {
      // Identifiers and non-walkables oddities like 'ThisExpression'
      if (extended) {
        current.end = extended.end;
        current.arguments = extended.arguments;
        extended = undefined;
        if (current.start >= awaitStart) {
          argsToEval.push(...current.arguments);
        }
      }
      chain.unshift(current);
    }
  }
  if (reverse) {
    chain = chain.sort((a, b) => {
      if (a.start === b.start) {
        return b.end - a.end;
      }
      return a.start - b.start;
    });
  }
  return {chain, awaitStart, argsToEval};
};

const getParentFunctions = (node, functionNodes) => {
  var functionArray = [].concat.apply([], Array.from(functionNodes.values()));
  const parentFunctions = functionArray
    .filter((obj) => {
      return obj.start < node.start && obj.end > node.start;
    })
    .sort((a, b) => {
      if (a.start === b.start) {
        return b.end - a.end;
      }
      return a.start - b.start;
    });
  return parentFunctions;
};

const getParents = (node) => {
  let parents = [];
  let current = node;
  while (current.parent) {
    if (current.parents) {
      parents = current.parents.concat(parents);
      break;
    }
    parents.unshift(current.parent);
    current = current.parent;
  }
  return parents;
};

// Gets the value that is immediately being referenced
const getAssignedValue = (node, functionOnly) => {
  if (
    isFunction(node.parent) &&
    node.parent.id &&
    scopeAnalyzer.matchNodes(node, node.parent.id)
  ) {
    return node.parent;
  }
  const parentAssignment = scopeAnalyzer.getParentAssignment(node);
  if (
    parentAssignment &&
    !parentAssignment.isGiver &&
    (!functionOnly || isFunction(parentAssignment.giver))
  ) {
    return parentAssignment.giver;
  }
  return undefined;
};

const isReturn = (node) => {
  let current = node.parent;
  let returnFound = false;
  while (current.parent) {
    if (
      current.type === 'AwaitExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      current = current.parent;
    } else {
      if (current.type === 'ReturnStatement') {
        returnFound = true;
      }
      break;
    }
  }
  return returnFound;
};

const getLeafIdentifiers = (node) => {
  let identifiers = [];
  let remaining = [node];
  while (remaining.length) {
    let current = remaining.pop();
    if (current.type === 'Identifier') {
      identifiers.push(current);
    } else if (current.type === 'AwaitExpression') {
      remaining.push(current.argument);
    } else if (
      current.type === 'ChainExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      remaining.push(current.expression);
    } else if (current.type === 'CallExpression') {
      remaining.push(current.callee);
    } else if (current.type === 'MemberExpression') {
      // TODO H - If computed, all assigned string values to the prop need to be retrieved (whether a Literal, Identifier, or MemberExpression, or a recursive mix)
      //        - Check use cases to make sure this needs to be done
      identifiers.push(current.property);
    } else {
      let assignedIds = getAssignedIdentifiers(current);
      identifiers.push(...assignedIds);
    }
  }
  return identifiers;
};

const getFunctionIdentifiers = (node) => {
  let identifiers = [];
  if (isFunction(node)) {
    if (node.id) {
      let leafIdentifiers = getLeafIdentifiers(node.id);
      identifiers.push(...leafIdentifiers);
    } else {
      let current = node;
      while (current.parent) {
        let parentNode = current.parent;
        if (scopeAnalyzer.isAssignmentNode(parentNode)) {
          var assignedNode = parentNode.left || parentNode.id || parentNode.key;
          let leafIdentifiers = getLeafIdentifiers(assignedNode);
          identifiers.push(...leafIdentifiers);
          break;
        } else if (
          parentNode.type === 'AwaitExpression' ||
          parentNode.type === 'ExpressionStatement'
        ) {
          current = current.parent;
        } else {
          break;
        }
      }
    }
  }
  return identifiers;
};

// const getObjectFromProp = (node) => {
//   // TODO - Extrapolate from Property.key to the full receiver (i.e. x = {y: 'val'} => x.y = 'val')
//   //        - while-recurse to handle nested object assignment until either AssignmentExpression or VariableDeclarator
//   let name = node.key.name || node.key.value || node.key.type;
//   let current = node;
//   while (current.parent) {
//     if (current.type === 'Property') {
//       let propName = node.key.name || node.key.value || node.key.type;
//       name = propName + name;
//     }
//     current = current.parent;
//   }
// };

const getBindingIds = (node, path, ASTs) => {
  if (!node) {
    return [];
  }
  let references = [];
  if (references.getReferences) {
    references.push(node.getReferences());
  } else if (!Array.isArray(references)) {
    let identifiers = isFunction(node) ? getFunctionIdentifiers(node) : [node];
    for (var i = 0; i < identifiers.length; i++) {
      let identifier = identifiers[i];
      let bindings = scopeAnalyzer.getBindings(identifier, path, ASTs);
      let bindingRefs = bindings.map((el) => el.getReferences());
      if (!bindingRefs.length) {
        let nodeId = identifier.type + identifier.start + identifier.end;
        return [nodeId];
      }
      references.push(...bindingRefs);
    }
  } else {
    references.push(node);
  }
  let results = [];
  for (var i = 0; i < references.length; i++) {
    let ref = references[i];
    if (ref[0]) {
      let refId = ref[0].type + ref[0].start + ref[0].end;
      results.push(refId);
    }
  }
  return results;
};

const tagNodes = (sourcePath, apis, jsContent, ASTs) => {
  var newContent = jsContent[sourcePath]
    .replace(
      /console\.log\((["'`]|JSON.stringify\()/g,
      'ReactNativeWebView.postMessage("[WEBVIEW-LOG] " + $1',
    )
    .replace(
      /console\.debug\((["'`]|JSON.stringify\()/g,
      'ReactNativeWebView.postMessage("[WEBVIEW-DEBUG] " + $1',
    )
    .replace(
      /console\.error\((["'`]|JSON.stringify\()/g,
      'ReactNativeWebView.postMessage("[WEBVIEW-ERROR] " + $1',
    )
    .replace(/\b(window)\b\./g, '');
  jsContent[sourcePath] = newContent;
  var initialNode = acornParse(newContent);
  var nodeWithParents = estreeAssignParent(initialNode);
  var globals = acornGlobals(initialNode);
  const apiBases = Object.keys(apis).concat(['fetch']);
  var apiNodes = globals.filter((obj) => apiBases.includes(obj.name));
  var nodejsExportNodes = globals.filter((obj) => {
    return (
      obj.name === 'modules' || obj.name === 'exports' || obj.name === 'require'
    );
  });
  scopeAnalyzer.createScope(nodeWithParents, Object.keys(globals));
  ASTs[sourcePath] = {
    // All nodes from parser
    nodes: nodeWithParents,
    // Map of function-defining nodes
    functionNodes: new Map(),
    // Nodes that should be awaitified or asyncified
    modifiedNodes: [],
    // Ids of the first node of a binding that's meant to be modified
    modifiedBindings: [],
    // Ids of bindings that are derived from imports
    importedIds: {},
    // Ids of nodes that have already been evaluated in this file's context
    evaluatedIds: [],
  };
  scopeAnalyzer.crawl(sourcePath, ASTs);
  var nodesToEvaluate = [];
  var altSourcePaths = {};

  const recordFunctionNode = (node) => {
    const identifiers = getFunctionIdentifiers(node);
    for (var i = 0; i < identifiers.length; i++) {
      let identifier = identifiers[i];
      if (identifier && identifier.type === 'Identifier') {
        let val = ASTs[sourcePath].functionNodes.get(identifier.name);
        if (val) {
          val.push(node);
          ASTs[sourcePath].functionNodes.set(identifier.name, val);
        } else {
          ASTs[sourcePath].functionNodes.set(identifier.name, [node]);
        }
      }
    }
  };
  const recordImport = (node) => {
    if (node.specifiers) {
      var importPaths = scopeAnalyzer.getAllStringValues(
        node.source,
        sourcePath,
        ASTs,
        true,
      );
      importPaths.forEach((path, i) => {
        let sourceExports = ASTs[path].fileExports;
        if (sourceExports) {
          node.specifiers.forEach((specifier, j) => {
            let local = specifier.local;
            let imported;
            if (specifier.type === 'ImportDefaultSpecifier') {
              imported = sourceExports.default
                ? [path, 'fileExports', 'default']
                : sourceExports.named
                ? [path, 'fileExports', 'named']
                : undefined;
            }
            if (specifier.type === 'ImportNamespaceSpecifier') {
              imported = sourceExports.named
                ? [path, 'fileExports', 'named']
                : sourceExports.default
                ? [path, 'fileExports', 'default']
                : undefined;
            }
            if (specifier.type === 'ImportSpecifier') {
              imported =
                sourceExports.named &&
                sourceExports.named[specifier.imported.name]
                  ? [path, 'fileExports', 'named', specifier.imported.name]
                  : sourceExports.default &&
                    sourceExports.default[specifier.imported.name]
                  ? [path, 'fileExports', 'default', specifier.imported.name]
                  : undefined;
            }
            if (imported) {
              let localBindings = scopeAnalyzer.getBindings(
                local,
                sourcePath,
                ASTs,
              );
              for (var j = 0; j < localBindings.length; j++) {
                let localBinding = localBindings[i];
                localBinding.getReferences().forEach((ref) => {
                  let refId = ref.type + ref.start + ref.end;
                  let newImports = ASTs[sourcePath].importedIds[refId] || [];
                  newImports.push(imported);
                  ASTs[sourcePath].importedIds[refId] = newImports;
                });
              }
            }
          });
        }
      });
    } else if (node.type === 'ImportExpression') {
      let assignment = scopeAnalyzer.getParentAssignment(node);
      if (assignment && assignment.isGiver) {
        // TODO H - Add to importedIds
        //        - UPDATE: Re-evaluate, simplify and clean up the actual importing system
        nodesToEvaluate.push(...assignment.receivers);
      }
    }
  };
  const recordExport = (node) => {
    // TODO H - List export in a manner that allows for easily finding its bindings
    //        - IDEA: {default: NODE, named: {name: NODE}}
    //        -     : Just passing the node would allow for imports to be evaluated dynamically
    //        -     : 'import *' means import 'named'
    let exportFromPaths = [sourcePath];
    if (node.source) {
      exportFromPaths = scopeAnalyzer.getAllStringValues(
        node.source,
        sourcePath,
        ASTs,
        true,
      );
    }
    if (!ASTs[sourcePath].fileExports) {
      ASTs[sourcePath].fileExports = {};
    }
    exportFromPaths.forEach((path) => {
      if (!ASTs[path]) {
        ASTs[path] = {fileExports: {}};
      }
      if (!ASTs[path].fileExports) {
        ASTs[path].fileExports = {};
      }
      if (node.type === 'ExportAllDeclaration') {
        if (!ASTs[sourcePath].fileExports.named) {
          ASTs[sourcePath].fileExports.named = {};
        }
        if (node.exported) {
          // TODO H - Treat 'exported' as an imported reference and add this to named
        } else {
          const importKeys = Object.keys(ASTs[path].fileExports.named);
          importKeys.forEach((key) => {
            ASTs[sourcePath].fileExports.named[key] = [
              path,
              'fileExports',
              'name',
              key,
            ];
          });
        }
      }
      if (node.type === 'ExportDefaultDeclaration') {
        ASTs[sourcePath].fileExports.default = node.declaration;
      }
      if (node.type === 'ExportNamedDeclaration') {
        // ExportSpecifier: 'local' = Identification in exporting file, 'exported' = Identification for importers
        if (node.declarations && node.declarations.length) {
          // TODO H - Iterate and record named exports
        }
        if (node.specifiers && node.specifiers.length) {
          // TODO H - Iterate and record named exports (if .local or .exported is 'default' then it's the default)
        }
      }
    });
  };
  acornWalk.simple(ASTs[sourcePath].nodes, {
    FunctionExpression: recordFunctionNode,
    ArrowFunctionExpression: recordFunctionNode,
    FunctionDeclaration: recordFunctionNode,
    ImportDeclaration: recordImport,
    // ImportDefaultSpecifier: recordImport,
    // ImportNamespaceSpecifier: recordImport,
    // ImportSpecifier: recordImport,
    ImportExpression: recordImport,
    ExportNamedDeclaration: recordExport,
    // ExportSpecifier: recordExport,
    ExportDefaultDeclaration: recordExport,
    ExportAllDeclaration: recordExport,
  });

  // Sends receiver of presumably awaited/asynced value to have its bindings be evaluated
  const evaluateParentAssignments = (assignment) => {
    if (assignment) {
      nodesToEvaluate.push(...assignment.receivers);
    }
  };

  const evaluateCallArgs = (nodes, path = sourcePath) => {
    let args = nodes.slice();
    while (args.length) {
      let arg = args.pop();
      if (isFunction(arg)) {
        nodesToEvaluate.push(refFunc);
        if (arg.params) {
          arg.params.forEach(function (param) {
            let assignedIds = getAssignedIdentifiers(param);
            nodesToEvaluate.push(...assignedIds);
          });
        }
      } else if (arg.type === 'Identifier') {
        var bindings = scopeAnalyzer.getBindings(arg, path, ASTs);
        for (var i = 0; i < bindings.length; i++) {
          let binding = bindings[i];
          const references = binding.getReferences();
          for (var i = 0; i < references.length; i++) {
            var ref = references[i];
            var refFunc = getAssignedValue(ref, true);
            if (refFunc) {
              nodesToEvaluate.push(refFunc);
              if (arg.params) {
                arg.params.forEach(function (param) {
                  var assignedIds = getAssignedIdentifiers(param);
                  nodesToEvaluate.push(...assignedIds);
                });
              }
            }
          }
        }
      } else {
        let assignedIds = getAssignedIdentifiers(arg);
        args.push(...assignedIds);
      }
    }
  };

  const evaluateParam = (node, path = sourcePath) => {
    let current = node.parent;
    while (current.parent) {
      if (
        current.type === 'AwaitExpression' ||
        current.type === 'ExpressionStatement'
      ) {
        current = current.parent;
      } else if (current.type === 'CallExpression') {
        current.arguments.forEach((arg, argIndex) => {
          if (scopeAnalyzer.matchNodes(node, arg)) {
            let callingIdentifiers = getLeafIdentifiers(current);
            for (var i = 0; i < callingIdentifiers.length; i++) {
              let callingIdentifier = callingIdentifiers[i];
              if (
                callingIdentifier &&
                callingIdentifier.name &&
                ASTs[path].functionNodes.has(callingIdentifier.name)
              ) {
                // TODO M - Limit to the nearest function in scope
                ASTs[path].functionNodes
                  .get(callingIdentifier.name)
                  .forEach((obj) => {
                    if (obj.params && obj.params[argIndex]) {
                      let argIdentifiers = getLeafIdentifiers(
                        obj.params[argIndex],
                      );
                      nodesToEvaluate.push(...argIdentifiers);
                    }
                  });
              }
            }
          }
        });
        break;
      } else {
        break;
      }
    }
  };

  const evaluateImportReference = (bindingIds, path = sourcePath) => {
    let importRefs = ASTs[path] ? ASTs[path].importedIds : undefined;
    if (importRefs) {
      for (var i = 0; i < bindingIds.length; i++) {
        let bindingId = bindingIds[i];
        let pathsToExportedNodes = importRefs[bindingId];
        if (pathsToExportedNodes) {
          pathsToExportedNodes.forEach((arrayPathToNode) => {
            let filePath = arrayPathToNode[0];
            let exportNode = ASTs;
            arrayPathToNode.forEach((subPath) => {
              if (exportNode) {
                exportNode = exportNode[subPath];
              }
            });
            if (filePath && exportNode) {
              let id = exportNode.type + exportNode.start + exportNode.end;
              altSourcePaths[id] = filePath;
              nodesToEvaluate.push(exportNode);
            }
          });
        }
      }
    }
  };

  const evaluateBinding = (node, isApi, path = sourcePath) => {
    console.log('[NODE] Binding Node:', node);
    var parents = node.parents || getParents(node);
    const fullNode =
      parents.filter((obj) => obj.start === node.start)[0] || node;
    const parentFunctions = getParentFunctions(
      fullNode,
      ASTs[path].functionNodes,
    );
    const id = fullNode.type + fullNode.start + fullNode.end;
    if (
      parentFunctions.length &&
      ASTs[path] &&
      !ASTs[path].evaluatedIds.includes(id)
    ) {
      ASTs[path].evaluatedIds.push(id);

      let parentAssignment = scopeAnalyzer.getParentAssignment(fullNode);
      if (!parentAssignment || parentAssignment.isGiver) {
        evaluateParentAssignments(parentAssignment);
        evaluateParam(fullNode, path);

        const isReturnNode = isReturn(fullNode);
        let closestFunction = parentFunctions.pop();
        // closestFunction.hasReturn = isReturnNode;
        let functionId =
          closestFunction.type + closestFunction.start + closestFunction.end;
        if (
          (!closestFunction.async || isReturnNode) &&
          ASTs[path] &&
          !ASTs[path].evaluatedIds.includes(functionId)
        ) {
          ASTs[path].evaluatedIds.push(functionId);
          nodesToEvaluate.push(closestFunction);
        }

        let {chain, awaitStart, argsToEval} = getIdentifierChain(
          fullNode,
          true,
        );
        evaluateCallArgs(argsToEval, path);

        if (!isApi) {
          ASTs[path].modifiedNodes.push({chain, awaitStart});
        }
      }
    }
  };

  for (var i = 0; i < apiNodes.length; i++) {
    const {name, nodes} = apiNodes[i];
    let bindingIds = getBindingIds(nodes[0], sourcePath, ASTs);
    ASTs[sourcePath].modifiedBindings.push(...bindingIds);
    console.log('[NODE] Iterating on API nodes for key:', name);
    for (var j = 0; j < nodes.length; j++) {
      const apiNode = nodes[j];
      evaluateBinding(apiNode, true);
    }
  }
  for (var i = 0; i < nodejsExportNodes.length; i++) {
    const {name, nodes} = nodejsExportNodes[i];
    console.log('[NODE] Iterating on export nodes for key:', name);
    for (var j = 0; j < nodes.length; j++) {
      const exportNode = nodes[j];
      let exportedValue;
      let importedValue;
      if (
        name === 'exports' &&
        exportNode.parent.parent.type === 'AssignmentExpression'
      ) {
        exportedValue = exportNode.parent.parent.right;
      }
      if (
        name === 'module' &&
        exportNode.parent.type === 'MemberExpression' &&
        exportNode.parent.property.name === 'exports' &&
        exportNode.parent.parent.type === 'AssignmentExpression'
      ) {
        exportedValue = exportNode.parent.parent.right;
      }
      if (exportedValue) {
        // TODO H - Handle
      }
      if (name === 'require' && exportNode.parent.type === 'CallExpression') {
        let importPaths = scopeAnalyzer.getAllStringValues(
          exportNode.parent.arguments[0],
          sourcePath,
          ASTs,
          true,
        );
        importPaths.forEach((path) => {
          if (ASTs[path] && ASTs[path].fileExports) {
            // TODO H - Require from source's module.exports
            //        - Treat the require itself as the imported node (i.e. If it's a function, the require could be called)
            //        - Walk assigned ObjectExpression(?) for the names in it
          }
        });
      }
    }
  }
  while (nodesToEvaluate.length) {
    let node = nodesToEvaluate.pop();
    var id = node.type + node.start + node.end;
    var path = altSourcePaths[id] || sourcePath;
    if (isFunction(node)) {
      console.log('[NODE] Function Node to Asyncify:', node);
      let bindingIds = getBindingIds(node, path, ASTs);
      ASTs[path].modifiedBindings.push(...bindingIds);
      ASTs[path].modifiedNodes.push(node);

      // Evaluate all calls to this function
      if (node.type === 'FunctionDeclaration' && node.id) {
        nodesToEvaluate.push(node.id);
      } else {
        var parentAssignment = scopeAnalyzer.getParentAssignment(node);
        if (parentAssignment) {
          evaluateParentAssignments(parentAssignment);
        }
      }
    } else if (node.getReferences) {
      let references = node.getReferences();
      let bindingIds = getBindingIds(references, path, ASTs);
      if (bindingIds.length) {
        ASTs[path].modifiedBindings.push(...bindingIds);
        evaluateImportReference(bindingIds);
      }
      references.forEach(function (ref) {
        evaluateBinding(ref, false, path);
      });
    } else if (node.type === 'Identifier' || node.type === 'Literal') {
      var bindings = scopeAnalyzer.getBindings(node, path, ASTs);
      if (bindings.length) {
        nodesToEvaluate.push(...bindings);
      } else if (
        node.type === 'Identifier' &&
        ASTs[path] &&
        !ASTs[path].evaluatedIds.includes(id)
      ) {
        // If there is no binding (like in the case of 'this.attribute'), crawl entire AST for any identifiers whose name matches this identifier and also don't have a binding
        acornWalk.full(ASTs[sourcePath].nodes, (walkNode) => {
          var walkId = walkNode.type + walkNode.start + walkNode.end;
          if (
            walkNode.type === node.type &&
            walkNode.name === node.name &&
            ASTs[path] &&
            !ASTs[path].evaluatedIds.includes(walkId) &&
            !scopeAnalyzer.getBindings(walkNode, path, ASTs)
          ) {
            evaluateBinding(walkNode, false, path);
          }
        });
        evaluateBinding(node, false, path);
      }
    } else {
      console.log('[NODE] Potential Binding Node:', node);
      let identifiers = getLeafIdentifiers(node);
      nodesToEvaluate.push(...identifiers);
    }
  }
  return newContent;
};

const getOffset = (position, offsets) => {
  let offset = 0;
  for (var i = 0; i < offsets.length; i++) {
    let pastOffset = offsets[i];
    if (pastOffset.pos <= position) {
      offset += pastOffset.disp;
    }
  }
  return offset;
};

const offsetValue = (value, offsets) => {
  let valueOffset = getOffset(value, offsets);
  let newValue = value + valueOffset;
  return newValue;
};

const offsetPosition = (start, end, offsets) => {
  let newStart = offsetValue(start, offsets);
  let newEnd = offsetValue(end, offsets);
  return {start: newStart, end: newEnd};
};

const editContentSection = (start, end, pre, post, content) => {
  let section = content.substring(start, end);
  let newSection = pre + section + post;
  let newContent =
    content.substring(0, start) + newSection + content.substring(end);
  return newContent;
};

const asyncify = (node, content, offsets) => {
  let {start, end} = offsetPosition(node.start, node.end, offsets);
  let newContent = editContentSection(start, end, 'async ', '', content);
  offsets.push({pos: node.start, disp: 6});
  return newContent;
};

const awaitify = ({chain, awaitStart}, content, offsets) => {
  let newContent = content;
  for (var i = 0; i < chain.length; i++) {
    let node = chain[i];
    let isLast = i === 0;
    let chainStart = offsetValue(chain[chain.length - 1].start);
    let {start, end} = offsetPosition(node.start, node.end, offsets);
    let offsetAwaitStart = offsetValue(awaitStart, offsets);
    if (start >= offsetAwaitStart) {
      let preString = isLast ? 'await ' : '(await ';
      let postString = isLast ? '' : ')';
      newContent = editContentSection(
        chainStart,
        end,
        preString,
        postString,
        newContent,
      );
      if (postString.length) {
        offsets.push({pos: node.end, disp: postString.length});
      }
      offsets.push({
        pos: chain[chain.length - 1].start,
        disp: preString.length,
      });
    }
  }
  return newContent;
};

const modifyContent = (content, nodes) => {
  let newContent = content;
  let offsets = [];
  let nodesToModify = nodes.sort((a, b) => {
    let aStart = a.awaitStart || a.start;
    let bStart = b.awaitStart || b.start;
    if (aStart === bStart) {
      let aEnd = a.chain[0].end || a.end;
      let bEnd = b.chain[0].end || b.end;
      return bEnd - aEnd;
    }
    return aStart - bStart;
  });
  while (nodesToModify.length) {
    let current = nodesToModify.pop();
    if (isFunction(current)) {
      newContent = asyncify(current, newContent, offsets);
    } else {
      newContent = awaitify(current, newContent, offsets);
    }
  }
  newContent = newContent
    .replace(/(async[\s]+){2,}/g, 'async ')
    .replace(/(await[\s]+){2,}/g, 'await ')
    .replace(/new[\n \s]+await[\s]/g, 'await new ');
  return newContent;
};

const parseAndWriteJS = (apis, modifyQueue, jsContent) => {
  var ASTs = {modifiedIds: []};
  var alreadyModified = {};
  var writePromises = [];
  for (var i = 0; i < modifyQueue.length; i++) {
    let filePath = modifyQueue[i];
    if (!alreadyModified[filePath] && jsContent[filePath]) {
      alreadyModified[filePath] = true;
      tagNodes(filePath, apis, jsContent, ASTs);
    }
  }
  for (var i = 0; i < modifyQueue.length; i++) {
    let filePath = modifyQueue[i];
    if (alreadyModified[filePath] && jsContent[filePath]) {
      alreadyModified[filePath] = false;

      var content = jsContent[filePath];
      var newContent = modifyContent(content, ASTs[filePath].modifiedNodes);

      let writeFile = new Promise((resolve, reject) => {
        var writeStream = fse.createWriteStream(filePath);
        writeStream.write(newContent, 'utf8');
        writeStream.on('finish', function () {
          console.log('[NODE] Finished Write Stream for path:', filePath);
          resolve();
        });
        writeStream.on('error', function (err) {
          console.log('[NODE] Write Stream Error:', err);
          reject(err);
        });
        writeStream.end();
      });
      writePromises.push(writeFile);
    }
  }
  return Promise.allSettled(writePromises);
};

module.exports = {
  parseAndWriteJS,
};
