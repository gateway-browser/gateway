import * as common from './common';
import * as browser from './browser';

export const ActionCreators = Object.assign({}, common, browser);
